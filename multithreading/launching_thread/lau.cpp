#include <iostream>
#include <mutex>
#include <thread>
#include <vector>

std::mutex output_mtx;
std::mutex run_thread_mtx;

struct fun_str
{
    void operator()()
    {
        std::lock_guard<std::mutex> lg{output_mtx};
        std::cerr << "fun_str thread!\n";
    }
};

void fun(int& k)
{
    std::lock_guard<std::mutex> lg{output_mtx};
    k = 3;
    std::cerr << k << " fun thread!\n";
}

void run_thread(std::thread& th)
{
    static int i = 0;
    th = std::thread{[l=i]()
        {
            std::lock_guard<std::mutex> lg{output_mtx};
            std::cerr << l << " thread from run_thread!\n";
        }
    };
    ++i;
}

int main()
{
    int k = 10;
    std::thread th1{fun, std::ref(k)};
    std::thread th2{fun_str{}};
    std::thread th3{[&output_mtx=output_mtx]()
        {
            std::lock_guard<std::mutex> lg{output_mtx};
            std::cerr << "lambda thread!\n";
        }
    };
    th1.join();
    th2.join();
    th3.join();
    std::vector<std::thread> v;
    const int nbr_of_threads = 3;
    for (int j = 0; j < nbr_of_threads; ++j) {
        std::thread th;
        run_thread(th);
        v.push_back(std::move(th));
    }
    for (auto& th : v) {
        if (th.joinable()) {
            th.join();
        }
    }
}

