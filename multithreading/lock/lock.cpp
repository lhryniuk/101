#include <array>
#include <iostream>
#include <mutex>
#include <queue>
#include <random>
#include <thread>
#include <vector>

constexpr int N = 100;

std::array<bool, N> exists;
std::mutex exists_m;

int count = 0;
std::mutex count_m;

std::queue<int> q;
std::mutex q_m;

void save()
{
    std::lock_guard<std::mutex> lg{q_m};
    if (not q.empty()) {
        const auto value = q.front();
        std::cout << "Consuming " << value << " in " << std::this_thread::get_id() << std::endl;
        // locks both mutexes to save information that value appeard in both
        // count (number of values) and exists variables
        std::lock(exists_m, count_m);
        std::lock_guard<std::mutex>(exists_m, std::adopt_lock);
        std::lock_guard<std::mutex>(count_m, std::adopt_lock);
        ++count;
        exists[value] = true;
        std::cout << "count = " << count << std::endl;
        q.pop();
    }
}

std::random_device rd;
std::mt19937 gen(rd());
std::uniform_int_distribution<> dis(1, N - 1);

void produce()
{
    std::lock_guard<std::mutex> lg{q_m};
    const auto value = dis(gen);
    std::cout << "Producing " << value << " in " << std::this_thread::get_id() << std::endl;
    q.push(value);
}

int main()
{
    std::vector<std::thread> producers;
    std::vector<std::thread> consumers;
    for (auto i = 0; i < N; ++i) {
        producers.emplace_back(produce);
        consumers.emplace_back(save);
    }
    for (auto& p : producers) {
        p.join();
    }
    for (auto& c : consumers) {
        c.join();
    }
}
