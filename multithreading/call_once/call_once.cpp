#include <iostream>
#include <memory>
#include <mutex>
#include <thread>

struct A
{
    A(int x) :x_{x} {}
    void print_x()
    {
        std::cout << x_ << std::endl;
    }
    const int x_;
};

std::once_flag resource_flag;
std::shared_ptr<A> resource_ptr;

void init_resource()
{
    resource_ptr = std::make_shared<A>(10);
}

void do_s()
{
    std::call_once(resource_flag, init_resource);
    resource_ptr->print_x();
}


int main()
{
    constexpr int n = 10;
    for (int i = 0; i < n; ++i)
    {
        std::thread t(do_s);
        t.join();
    }
}
