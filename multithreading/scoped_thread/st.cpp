#include <exception>
#include <stdexcept>
#include <iostream>
#include <thread>

namespace st
{


class scoped_thread
{
public:
    explicit scoped_thread(std::thread th)
        : th_{std::move(th)}
    {
        if (!th_.joinable()) {
            throw std::logic_error("No thread");
        };
    }
    scoped_thread(const scoped_thread&)=delete;
    scoped_thread& operator=(const scoped_thread&)=delete;
    ~scoped_thread()
    {
        th_.join();
    }
private:
    std::thread th_;
};


}

int main()
{
    {
        st::scoped_thread st{std::thread{[](){std::cout << "Hello\n";}}};
    }
}
