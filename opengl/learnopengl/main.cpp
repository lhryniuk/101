#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <stdexcept>


std::string load_shader_source(std::string filename);
void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow *window);
int loadVertexShader(const std::string& fshaderPath);
int loadFragmentShader(const std::string& fshaderPath);
GLFWwindow* initGlfw();
int linkProgram(int vertexShader, int fragmentShader);


const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;
const std::string fragmentShaderPath = "fshader.glsl";
const std::string fragmentShaderPath2 = "fshader2.glsl";
const std::string vertexShaderPath = "vshader.glsl";

float vertices[] = {
     1.0f,  1.0f, 0.0f, 1.0f, 0.0f, 0.0f, // top right
     1.0f, -0.5f, 0.0f, 0.0f, 1.0f, 0.0f, // bottom right
     0.0f,  0.5f, 0.0f, 0.0f, 0.0f, 1.0f // bottom left
};

float verticesp[] = {
    -0.5f, -0.8f, 0.0f, 1.0f, 0.0f, 0.0f, // bottom left
    -0.5f,  0.5f, 0.0f, 0.0f, 1.0f, 0.0f, // top left
    -1.0f,  0.5f, 0.0f, 0.0f, 0.0f, 1.0f  // top left
};

unsigned int indices[] = {  // note that we start from 0!
    0, 1, 2,  // first Triangle
};


int main()
{
    GLFWwindow* window = initGlfw();

    // glewExperimental = GL_TRUE;
    glewInit();

    const auto vertexShader = loadVertexShader(vertexShaderPath);
    const auto fragmentShader = loadFragmentShader(fragmentShaderPath);
    const auto shaderProgram = linkProgram(vertexShader, fragmentShader);

    const auto fragmentShader2 = loadFragmentShader(fragmentShaderPath2);
    const auto shaderProgram2 = linkProgram(vertexShader, fragmentShader2);

    GLuint VAO[2];
    glGenVertexArrays(2, VAO);
    GLuint VBO[2];
    glGenBuffers(2, VBO);
    GLuint EBO;
    glGenBuffers(1, &EBO);

    glBindVertexArray(VAO[0]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3* sizeof(float)));
    glEnableVertexAttribArray(1);

    glBindVertexArray(VAO[1]);
    glBindBuffer(GL_ARRAY_BUFFER, VBO[1]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(verticesp), verticesp, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3* sizeof(float)));
    glEnableVertexAttribArray(1);

    // note that this is allowed, the call to glVertexAttribPointer registered VBO as the vertex attribute's bound vertex buffer object so afterwards we can safely unbind
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    glBindVertexArray(0);

    while (!glfwWindowShouldClose(window))
    {
        processInput(window);

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);


        GLint timeValueLocation = glGetUniformLocation(shaderProgram, "timeValue");
        glUseProgram(shaderProgram);

        if (timeValueLocation == -1) {
            std::cout << "cannot find location of timeValue" << std::endl;
        } else {
            glUniform1f(timeValueLocation, glfwGetTime());
        }

        glBindVertexArray(VAO[0]);
        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0);

        glUseProgram(shaderProgram2);
        glBindVertexArray(VAO[1]);
        glDrawElements(GL_TRIANGLES, 3, GL_UNSIGNED_INT, 0);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glDeleteVertexArrays(2, VAO);
    glDeleteBuffers(2, VBO);
    glDeleteBuffers(1, &EBO);

    glfwTerminate();
    return 0;
}

std::string load_shader_source(const std::string filename)
{
    std::ifstream in(filename);
    assert(in);
    std::stringstream source;
    source << in.rdbuf();
    return source.str();
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow* window, const int width, const int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

GLFWwindow* initGlfw()
{
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        glfwTerminate();
        throw std::runtime_error("Failed to create GLFW window");
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    return window;
}


int getCompiledShader(const std::string& shaderFilePath, const GLenum type)
{
    const int shader = glCreateShader(type);
    if (shader == 0)
    {
        throw std::runtime_error("Cannot create shader");
    }
    const auto src = load_shader_source(shaderFilePath);
    const auto src_c_cstr = src.c_str();
    glShaderSource(shader, 1, &src_c_cstr, NULL);
    glCompileShader(shader);

    int success{};
    char infoLog[512] = {};
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cout << "ERROR COMPILATION_FAILED: " << shaderFilePath<< "\n" << infoLog << std::endl;
    }

    return shader;
}


int loadVertexShader(const std::string& vshaderPath)
{
    return getCompiledShader(vshaderPath, GL_VERTEX_SHADER);
}


int loadFragmentShader(const std::string& fshaderPath)
{
    return getCompiledShader(fshaderPath, GL_FRAGMENT_SHADER);
}


int linkProgram(int vertexShader, int fragmentShader)
{
    int shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);

    int success{};
    char infoLog[512] = {};
    glGetProgramiv(shaderProgram, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    return shaderProgram;
}
