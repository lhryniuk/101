#version 330

in vec4 vertexColor;

out vec4 FragColor;

void main()
{
    FragColor = vec4(vertexColor.x * vertexColor.y, vertexColor.y, vertexColor.x, 1.0f);
}
