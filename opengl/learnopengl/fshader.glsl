#version 330

in vec4 vertexColor;

uniform float timeValue;

out vec4 FragColor;

void main()
{
    FragColor = vec4((sin(timeValue) / 2.0f + 0.5f) * vertexColor.xyz, 1.0f);
}
