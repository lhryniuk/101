#include <cassert>
#include <chrono>
#include <cmath>
#include <cstdio>
#include <fstream>
#include <iostream>
#include <sstream>
#include <thread>

#include <GL/glew.h>
#include <GLFW/glfw3.h>


GLFWwindow* init_glfw()
{
    // glfw init
    glfwInit();

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

    GLFWwindow* window = glfwCreateWindow(800, 600, "OpenGL", nullptr, nullptr); // Windowed
    glfwMakeContextCurrent(window);
    return window;
}

std::string load_shader_source(std::string filename)
{
    std::ifstream in(filename);
    assert(in);
    std::stringstream source;
    source << in.rdbuf();
    return source.str();
}

void compile_shader(GLuint& shader, std::string filename)
{
    auto str_src = load_shader_source(filename);
    const GLchar* src = str_src.c_str();
    glShaderSource(shader, 1,  &src, NULL);
    glCompileShader(shader);
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    char buffer[512];
    glGetShaderInfoLog(shader, 512, NULL, buffer);
    if (status != GL_TRUE) {
        printf("Error message: %s\n", buffer);
    }
}

void compile_vertex_shader(GLuint& shader)
{
    std::string filename("vshader.glsl");
    compile_shader(shader, filename);
}

void compile_fragment_shader(GLuint& shader)
{
    std::string filename("fshader.glsl");
    compile_shader(shader, filename);
}

int main()
{
    // glfw init
    auto window = init_glfw();

    // glew init
    glewExperimental = GL_TRUE;
    glewInit();

    // create and use Vertex Array Object
    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    GLfloat vertices[] = {
        -0.5f,  0.5f, 1.0f, 0.0f, 0.0f, // Top-left
         0.5f,  0.5f, 0.0f, 1.0f, 0.0f, // Top-right
         0.5f, -0.5f, 0.0f, 0.0f, 1.0f, // Bottom-right
        -0.5f, -0.5f, 1.0f, 1.0f, 1.0f  // Bottom-left
    };

    GLuint vbo;
    glGenBuffers(1, &vbo); // generate vertex buffer object

    // buffer object for elements
    GLuint ebo;
    glGenBuffers(1, &ebo);

    glBindBuffer(GL_ARRAY_BUFFER, vbo); // upload data and make it active
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    GLuint elements[] = {
        1, 2, 0,
        2, 3, 0
    };

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ebo);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(elements), elements, GL_STATIC_DRAW);

    // shaders
    GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
    compile_vertex_shader(vertexShader);

    GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    compile_fragment_shader(fragmentShader);

    // connecting shaders
    GLuint shaderProgram = glCreateProgram();
    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glBindFragDataLocation(shaderProgram, 0, "outColor");

    // linking
    glLinkProgram(shaderProgram);
    glUseProgram(shaderProgram);

    // prepare attributes
    GLint posAttrib = glGetAttribLocation(shaderProgram, "position");
    glEnableVertexAttribArray(posAttrib);
    glVertexAttribPointer(posAttrib, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);

    // prepare attributes
    GLint colAttrib = glGetAttribLocation(shaderProgram, "color");
    glEnableVertexAttribArray(colAttrib);
    glVertexAttribPointer(colAttrib, 3, GL_FLOAT, GL_FALSE,
                          5 * sizeof(float), (void*)(2 * sizeof(float)));

    // get access to triangleColor uniform
    GLint uniColor = glGetUniformLocation(shaderProgram, "triangleColor");

    auto t_start = std::chrono::high_resolution_clock::now();
    auto change_color = [&uniColor, &t_start]() {
        auto t_now = std::chrono::high_resolution_clock::now();
        float time = (t_now - t_start).count();
        float x = time / 1000000000.0f;
        glUniform3f(uniColor, sin(x), cos(x), cos(x) + 2.0f * sin(x));
    };

    while (!glfwWindowShouldClose(window)) {
        glfwPollEvents();
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetWindowShouldClose(window, GL_TRUE);
        // Clear the screen to black
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        // glDrawArrays(GL_TRIANGLES, 0, 3);
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        glfwSwapBuffers(window);
        // change_color();
    }
    glfwTerminate();
    return 0;
}
