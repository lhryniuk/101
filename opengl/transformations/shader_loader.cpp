#include <cassert>
#include <sstream>
#include <fstream>

#include "shader_loader.hpp"

std::string load_shader_source(std::string filename)
{
    std::ifstream in(filename);
    assert(in);
    std::stringstream source;
    source << in.rdbuf();
    return source.str();
}

void compile_shader(GLuint& shader, const std::string& filename)
{
    auto str_src = load_shader_source(filename);
    const GLchar* src = str_src.c_str();
    glShaderSource(shader, 1,  &src, NULL);
    glCompileShader(shader);
    GLint status;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
    char buffer[512];
    glGetShaderInfoLog(shader, 512, NULL, buffer);
    if (status != GL_TRUE) {
        printf("Error for file %s: %s\n", filename.c_str(), buffer);
    }
}

void compile_vertex_shader(GLuint& shader)
{
    std::string filename("vshader.glsl");
    compile_shader(shader, filename);
}

void compile_fragment_shader(GLuint& shader)
{
    std::string filename("fshader.glsl");
    compile_shader(shader, filename);
}
