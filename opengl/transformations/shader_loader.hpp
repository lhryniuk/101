#ifndef SHADER_LOADER_HPP
#define SHADER_LOADER_HPP

#include <string>

#include <GL/glew.h>

std::string load_shader_source(std::string filename);
void compile_shader(GLuint& shader, const std::string& filename);
void compile_vertex_shader(GLuint& shader);
void compile_fragment_shader(GLuint& shader);

#endif /* SHADER_LOADER_HPP */
