#version 150

in vec3 Color;
in vec2 Texcoord;

out vec4 outColor;

uniform int time;

uniform sampler2D texCow;
uniform sampler2D texBabes;

void main()
{
    const int mod1 = 200;
    const int mod2 = mod1 / 2;
    if (Texcoord.y < 0.5) {
        vec4 colCow = texture(texCow, Texcoord);
        vec4 colBabes = texture(texBabes, Texcoord);
        outColor = mix(colCow, colBabes, 1.0);
        // outColor = mix(colCow, colBabes, (abs(time % 200 - 100) % 100) / 100.0);
    } else {
        vec4 colCow = texture(texCow,
                              vec2(sin(Texcoord.y * ((time % mod1) / 10.0)) + Texcoord.x,
                                   1.0 - Texcoord.y));
        vec4 colBabes = texture(texBabes,
                                vec2(sin(Texcoord.y * (time / 10.0)) + Texcoord.x,
                                     1.0 - Texcoord.y));
        outColor = mix(colCow, colBabes, 1.0);
        // (abs(time % mod1 - mod2) % mod2) / 100.0);
    }
}
