// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/04/Fill.asm

// Runs an infinite loop that listens to the keyboard input. 
// When a key is pressed (any key), the program blackens the screen,
// i.e. writes "black" in every pixel. When no key is pressed, the
// program clears the screen, i.e. writes "white" in every pixel.


// end of SCREEN address: 24575

(FILLING)
  // Current pixel of SCREEN to fill at R0
  @R0
  M=0

  (FILLBIT)
    @SCREEN
    D=A // save SCREEN address in D
    @R0
    D=D+M // add offset
    A=D // set this address
    M=1 // fill bit
    @R0 // increment bit address
    M=M+1
    @KBD // check, if any key is pressed
    D=M
    @CLEANING
    D;JEQ
    @FILLBIT
    D;JNE

(CLEARING)
  // Current pixel of SCREEN to clear at R1
  @R1
  M=0
  (CLEARBIT)
    @SCREEN
    D=A
    @R1
    D=D+M
    A=D
    M=0
    @R1
    M=M+1
    @KBD
    D=M
    @FILLING
    D;JNE
    @CLEARBIT
    D;JEQ
