// This file is part of www.nand2tetris.org
// and the book "The Elements of Computing Systems"
// by Nisan and Schocken, MIT Press.
// File name: projects/01/Mux8Way16.hdl

/**
 * 8-way 16-bit multiplexor.
 * out = a if sel==000
 *       b if sel==001
 *       etc.
 *       h if sel==111
 */

CHIP Mux8Way16 {
    IN a[16], b[16], c[16], d[16],
       e[16], f[16], g[16], h[16],
       sel[3];
    OUT out[16];

    PARTS:
    // Selecting on 3 levels
    // I level
    Mux16(a=a, b=b, sel=sel[0], out=Ic);
    Mux16(a=c, b=d, sel=sel[0], out=IIc);
    Mux16(a=e, b=f, sel=sel[0], out=IIIc);
    Mux16(a=g, b=h, sel=sel[0], out=IVc);
    
    // II level
    Mux16(a=Ic, b=IIc, sel=sel[1], out=Id);
    Mux16(a=IIIc, b=IVc, sel=sel[1], out=IId);
    
    // III level
    Mux16(a=Id, b=IId, sel=sel[2], out=out);
}
