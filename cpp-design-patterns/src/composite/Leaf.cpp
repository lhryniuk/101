#include <exception>
#include <iostream>

#include "Leaf.hpp"

Leaf::Leaf(std::string name)
    : name_{name}
{
}

bool Leaf::DoWork()
{
    std::cerr << "Starting work of " << name_ << "\n";
    std::cerr << "Work, work, work!\n";
    std::cerr << "Finishing work of " << name_ << "\n";
    return true;
}

void Leaf::Add(std::shared_ptr<IWorkingUnitComponent> component_)
{
    throw std::exception{};
}

void Leaf::Remove(std::shared_ptr<IWorkingUnitComponent> component_)
{
    throw std::exception{};
}
