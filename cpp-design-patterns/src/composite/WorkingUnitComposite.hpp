#ifndef WORKING_UNIT_COMPOSITE_HPP
#define WORKING_UNIT_COMPOSITE_HPP

#include <vector>

#include "IWorkingUnitComponent.hpp"

class WorkingUnitComposite : public IWorkingUnitComponent
{
public:
    WorkingUnitComposite(std::string name);
    virtual bool DoWork() override;
    virtual void Add(std::shared_ptr<IWorkingUnitComponent> component_) override;
    virtual void Remove(std::shared_ptr<IWorkingUnitComponent> component_) override;

protected:
    std::string name_;
    std::vector<std::shared_ptr<IWorkingUnitComponent>> components;
};

#endif /* IWORKING_UNIT_COMPOSITE_HPP */
