#ifndef PROGRAMMER_HPP
#define PROGRAMMER_HPP

#include "Leaf.hpp"

class Programmer : public Leaf
{
public:
    using Leaf::Leaf;
};

#endif /* PROGRAMMER_HPP */
