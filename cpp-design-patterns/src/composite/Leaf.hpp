#ifndef LEAF_HPP
#define LEAF_HPP

#include "IWorkingUnitComponent.hpp"

class Leaf : public IWorkingUnitComponent
{
public:
    Leaf(std::string name);
    bool DoWork() override;
    virtual void Add(std::shared_ptr<IWorkingUnitComponent> component_) override;
    virtual void Remove(std::shared_ptr<IWorkingUnitComponent> component_) override;
private:
    std::string name_;
};

#endif /* LEAF_HPP */
