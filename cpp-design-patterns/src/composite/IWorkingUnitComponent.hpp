#ifndef IWORKING_UNIT_COMPONENT_HPP
#define IWORKING_UNIT_COMPONENT_HPP

#include <memory>

class IWorkingUnitComponent
{
public:
    virtual bool DoWork() = 0;
    virtual void Add(std::shared_ptr<IWorkingUnitComponent> component_) = 0;
    virtual void Remove(std::shared_ptr<IWorkingUnitComponent> component_) = 0;
};

#endif /* IWORKING_UNIT_COMPONENT_HPP */
