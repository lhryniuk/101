#include <algorithm>
#include <iostream>

#include "WorkingUnitComposite.hpp"


WorkingUnitComposite::WorkingUnitComposite(std::string name)
    : name_{name}
{
}

bool WorkingUnitComposite::DoWork()
{
    std::cerr << "Starting work of " << name_ << "\n";
    bool status{true};
    for (auto&& c : components) {
        status = c->DoWork() || status;
    }
    std::cerr << "Finishing work of " << name_ << "\n";
    return status;
}

void WorkingUnitComposite::Add(std::shared_ptr<IWorkingUnitComponent> component_)
{
    components.push_back(component_);
}

void WorkingUnitComposite::Remove(std::shared_ptr<IWorkingUnitComponent> component_)
{
    auto it = std::find(std::begin(components), std::end(components), component_);
    if (it != std::end(components)) {
        components.erase(it);
    }
}
