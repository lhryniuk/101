#include "Admin.hpp"
#include "Company.hpp"
#include "IWorkingUnitComponent.hpp"
#include "Leaf.hpp"
#include "Partnership.hpp"
#include "Programmer.hpp"
#include "Recruiter.hpp"
#include "Tester.hpp"
#include "WorkingUnitComposite.hpp"

std::unique_ptr<IWorkingUnitComponent> create_company()
{
    std::unique_ptr<IWorkingUnitComponent> partnership_ptr = std::make_unique<Partnership>("Partnership of companies");
    auto companies = std::vector<std::shared_ptr<IWorkingUnitComponent>>{
        std::make_shared<Company>("First company"),
        std::make_shared<Company>("Second company"),
        std::make_shared<Company>("Third company")
    };
    auto workers = std::vector<std::vector<std::shared_ptr<IWorkingUnitComponent>>>{
        {
            std::make_shared<Programmer>("First programmer"),
            std::make_shared<Programmer>("Second programmer"),
            std::make_shared<Tester>("First tester"),
            std::make_shared<Recruiter>("First recruiter")
        },
        {
            std::make_shared<Programmer>("First programmer"),
            std::make_shared<Tester>("First tester"),
            std::make_shared<Tester>("Second tester"),
            std::make_shared<Tester>("Third tester"),
            std::make_shared<Recruiter>("First recruiter")
        },
        {
            std::make_shared<Programmer>("First programmer"),
            std::make_shared<Tester>("First tester"),
            std::make_shared<Recruiter>("First recruiter")
        }
    };
    for (auto i = 0u; i < workers.size(); ++i) {
        for (const auto& worker : workers[i]) {
            companies[i]->Add(worker);
        }
    }
    for (auto& company : companies) {
        partnership_ptr->Add(company);
    }
    return partnership_ptr;
}

int main()
{
    auto p = create_company();
    p->DoWork();
    return 0;
}
