#ifndef COMPANY_HPP
#define COMPANY_HPP

#include "WorkingUnitComposite.hpp"

class Company : public WorkingUnitComposite
{
    using WorkingUnitComposite::WorkingUnitComposite;
};

#endif /* COMPANY_HPP */
