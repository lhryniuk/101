#pragma once

#include "AbstractTextDecorator.hpp"

class MarkdownHeadingDecorator : public AbstractTextDecorator
{
public:
    explicit MarkdownHeadingDecorator(std::unique_ptr<AbstractText> text)
        : AbstractTextDecorator(std::move(text))
    { }
    virtual std::string GetText() override
    {
        return "# " + AbstractTextDecorator::GetText();
    }
};
