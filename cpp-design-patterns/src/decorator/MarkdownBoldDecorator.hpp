#pragma once

#include "AbstractTextDecorator.hpp"

class MarkdownBoldDecorator : public AbstractTextDecorator
{
public:
    explicit MarkdownBoldDecorator(std::unique_ptr<AbstractText> text)
        : AbstractTextDecorator(std::move(text))
    { }
    virtual std::string GetText() override
    {
        return "**" + AbstractTextDecorator::GetText() + "**";
    }
};
