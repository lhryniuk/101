#pragma once

#include <string>

class AbstractText
{
public:
    virtual std::string GetText() = 0;
};
