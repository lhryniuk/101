#pragma once

#include "AbstractText.hpp"

#include <memory>

class AbstractTextDecorator : public AbstractText
{
public:
    explicit AbstractTextDecorator(std::unique_ptr<AbstractText> text)
        : text_{std::move(text)}
    { }
    virtual std::string GetText() override
    {
        return text_->GetText();
    }
private:
    std::unique_ptr<AbstractText> text_;
};
