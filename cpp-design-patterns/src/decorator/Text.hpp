#pragma once

#include "AbstractText.hpp"

class Text : public AbstractText
{
public:
    Text(std::string text)
        : text_{text}
    {}
    virtual std::string GetText() override
    {
        return text_;
    }
private:
    std::string text_;
};
