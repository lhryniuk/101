#pragma once

#include "AbstractTextDecoratorFactory.hpp"
#include "MarkdownBoldDecorator.hpp"
#include "MarkdownItalicDecorator.hpp"
#include "MarkdownHeadingDecorator.hpp"

class MarkdownDecoratorFactory : public AbstractTextDecoratorFactory
{
public:
    virtual std::unique_ptr<AbstractTextDecorator> Bold(std::unique_ptr<AbstractText> text) override
    {
        return std::make_unique<MarkdownBoldDecorator>(std::move(text));
    }
    virtual std::unique_ptr<AbstractTextDecorator> Italic(std::unique_ptr<AbstractText> text) override
    {
        return std::make_unique<MarkdownItalicDecorator>(std::move(text));
    }
    virtual std::unique_ptr<AbstractTextDecorator> Heading(std::unique_ptr<AbstractText> text) override
    {
        return std::make_unique<MarkdownHeadingDecorator>(std::move(text));
    }
};
