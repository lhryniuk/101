#include <iostream>

#include "HTMLDecoratorFactory.hpp"
#include "MarkdownDecoratorFactory.hpp"
#include "Text.hpp"

int main()
{
    std::unique_ptr<AbstractTextDecoratorFactory> mdf = std::make_unique<HTMLDecoratorFactory>();
    std::unique_ptr<AbstractTextDecorator> text{
        mdf->Heading(
            mdf->Italic(
                mdf->Bold(std::make_unique<Text>("elo"))))};
    std::cout << text->GetText() << "\n";
}
