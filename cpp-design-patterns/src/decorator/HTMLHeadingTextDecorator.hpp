#pragma once

#include "AbstractTextDecorator.hpp"

class HTMLHeadingTextDecorator : public AbstractTextDecorator
{
public:
    explicit HTMLHeadingTextDecorator(std::unique_ptr<AbstractText> text)
        : AbstractTextDecorator(std::move(text))
    { }
    virtual std::string GetText() override
    {
        return "<h1>" + AbstractTextDecorator::GetText() + "</h1>";
    }
};
