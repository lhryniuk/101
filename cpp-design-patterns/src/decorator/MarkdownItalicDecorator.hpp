#pragma once

#include "AbstractTextDecorator.hpp"

class MarkdownItalicDecorator : public AbstractTextDecorator
{
public:
    explicit MarkdownItalicDecorator(std::unique_ptr<AbstractText> text)
        : AbstractTextDecorator(std::move(text))
    { }
    virtual std::string GetText() override
    {
        return "_" + AbstractTextDecorator::GetText() + "_";
    }
};
