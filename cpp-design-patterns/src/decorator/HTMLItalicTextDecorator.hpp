#pragma once

#include "AbstractTextDecorator.hpp"

class HTMLItalicTextDecorator : public AbstractTextDecorator
{
public:
    explicit HTMLItalicTextDecorator(std::unique_ptr<AbstractText> text)
        : AbstractTextDecorator(std::move(text))
    { }
    virtual std::string GetText() override
    {
        return "<i>" + AbstractTextDecorator::GetText() + "</i>";
    }
};
