#pragma once

#include <memory>

#include "AbstractTextDecorator.hpp"

class AbstractTextDecoratorFactory
{
public:
    virtual std::unique_ptr<AbstractTextDecorator> Bold(std::unique_ptr<AbstractText> text) = 0;
    virtual std::unique_ptr<AbstractTextDecorator> Italic(std::unique_ptr<AbstractText> text) = 0;
    virtual std::unique_ptr<AbstractTextDecorator> Heading(std::unique_ptr<AbstractText> text) = 0;
};
