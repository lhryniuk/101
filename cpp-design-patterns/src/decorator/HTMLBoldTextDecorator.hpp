#pragma once

#include "AbstractTextDecorator.hpp"

class HTMLBoldTextDecorator : public AbstractTextDecorator
{
public:
    explicit HTMLBoldTextDecorator(std::unique_ptr<AbstractText> text)
        : AbstractTextDecorator(std::move(text))
    { }
    virtual std::string GetText() override
    {
        return "<b>" + AbstractTextDecorator::GetText() + "</b>";
    }
};
