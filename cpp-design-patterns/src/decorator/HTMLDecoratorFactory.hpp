#pragma once

#include "AbstractTextDecoratorFactory.hpp"
#include "HTMLBoldTextDecorator.hpp"
#include "HTMLItalicTextDecorator.hpp"
#include "HTMLHeadingTextDecorator.hpp"

class HTMLDecoratorFactory : public AbstractTextDecoratorFactory
{
public:
    virtual std::unique_ptr<AbstractTextDecorator> Bold(std::unique_ptr<AbstractText> text) override
    {
        return std::make_unique<HTMLBoldTextDecorator>(std::move(text));
    }
    virtual std::unique_ptr<AbstractTextDecorator> Italic(std::unique_ptr<AbstractText> text) override
    {
        return std::make_unique<HTMLItalicTextDecorator>(std::move(text));
    }
    virtual std::unique_ptr<AbstractTextDecorator> Heading(std::unique_ptr<AbstractText> text) override
    {
        return std::make_unique<HTMLHeadingTextDecorator>(std::move(text));
    }
};
