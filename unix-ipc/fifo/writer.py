import os
import stat

fifoname = 'fifo'

if __name__ == '__main__':
    os.mknod('fifo', stat.S_IFIFO | 0666)
    with open(fifoname, 'w') as q:
        for i in range(5):
            q.write(str(i) + '\n')
