import functools
import os
import signal
import sys
import time

def spawn_cpp_child(signal, frame):
    pid = os.fork()
    if pid == 0:
        os.execv("./child", [''])
    else:
        print("Spawned a cpp child {}!".format(pid))

def spawn_func_child(write_pipe, signal, frame):
    pid = os.fork()
    if pid == 0:
        child(write_pipe)
    else:
        print("Spawned a python child {}!".format(pid))

def parent():
    read_pipe, write_pipe = os.pipe()
    signal.signal(signal.SIGUSR1, spawn_cpp_child)
    signal.signal(signal.SIGUSR2, functools.partial(spawn_func_child, write_pipe))
    print(os.getpid())
    while True:
        time.sleep(0.1)
        if read_pipe is not None:
            print("Message from a child: {}".format(os.read(read_pipe, 10).decode()))

def child(write_pipe):
    for i in range(10):
        os.write(write_pipe, "New!".encode())
        time.sleep(0.1)

if __name__ == '__main__':
    parent()
