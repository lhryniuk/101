import fcntl
import os
import struct
import time


delay = 5
filename = 'a.lock'
with open(filename, 'w') as f:
    rv = fcntl.fcntl(f, fcntl.F_SETFL, os.O_NDELAY)

    lockdata = struct.pack('hhllhh', fcntl.F_WRLCK, 0, 0, 0, os.getpid(), 0)
    rv = fcntl.fcntl(f, fcntl.F_SETLKW, lockdata)
    print("locked")
    time.sleep(delay)
    unlockdata = struct.pack('hhllhh', fcntl.F_UNLCK, 0, 0, 0, os.getpid(), 0)
    rv = fcntl.fcntl(f, fcntl.F_SETLK, unlockdata)
    print("unlocked")
    time.sleep(delay)

