import fcntl
import struct
import time

lock_names = {
    fcntl.F_WRLCK: 'write',
    fcntl.F_RDLCK: 'read',
    fcntl.F_UNLCK: 'unlock'
}

filename = 'a.lock'
with open(filename, 'w') as f:
    f.write('xd')
    time.sleep(1)
    lockdata = struct.pack('hhllhh', fcntl.F_WRLCK, 0, 0, 0, 0, 0)
    rv = struct.unpack('hhllhh', fcntl.fcntl(f, fcntl.F_GETLK, lockdata))
    print(rv)
    if rv[0] == fcntl.F_UNLCK:
        print("file {} is not locked".format(filename))
    else:
        print("file {} is locked with {} lock by program with pid {}".format(
            filename, lock_names[rv[0]], rv[4]))
