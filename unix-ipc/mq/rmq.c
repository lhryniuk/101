#include <sys/msg.h>
#include <stdio.h>

const char* mq_path = "./mq";

int create_queue()
{
    const int key = ftok(mq_path, 1);
    return msgget(key, 0666);
}

struct imsg
{
    long mtype_;
    struct desc
    {
        int sender;
        int receiver;
        char msg[20];
    } desc_;
};


int recv_msg(int mq, struct imsg* msg)
{
    return msgrcv(mq, msg, sizeof(msg->desc_), 0, 0);
}


int main()
{
    const int mq = create_queue();
    struct imsg msg;
    recv_msg(mq, &msg);
    printf("%s\n", msg.desc_.msg);
    msgctl(mq, IPC_RMID, NULL);
}
