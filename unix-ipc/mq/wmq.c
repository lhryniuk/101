#include <sys/msg.h>
#include <stdlib.h>


const char* mq_path = "./mq";

int create_queue()
{
    const int key = ftok(mq_path, 1);
    return msgget(key, 0666 | IPC_CREAT);
}

struct imsg
{
    long mtype_;
    struct desc
    {
        int sender;
        int receiver;
        char msg[20];
    } desc_;
};


int send_msg(int mq, struct imsg* msg)
{
    return msgsnd(mq, msg, sizeof(msg->desc_), 0);
}


int main()
{
    const int mq = create_queue();
    struct imsg msg = {10, {10, 10, "xd"}};
    send_msg(mq, &msg);
    msgctl(mq, IPC_RMID, NULL);
}
