#include <arpa/inet.h>
#include <errno.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>
#include <sys/time.h>
#include <unistd.h>

#include "util.h"

const int PACKETS_TO_SEND = 3;
const int TTL_LIMIT = 30;
const double TIMEOUT = 100.0; // miliseconds

typedef struct packet_data {
    int seq;
    uint16_t checksum;
    bool recv;
    char src_ip_str[20];
    struct timespec send_time;
    struct timespec recv_time;
} packet_data;

void init_packet_data(packet_data* pd)
{
    pd->seq = -1;
    pd->recv = false;
    pd->send_time.tv_sec = -1;
    pd->send_time.tv_nsec = -1;
}

void init_packets_data(packet_data pds[TTL_LIMIT + 1][PACKETS_TO_SEND + 1])
{
    for (int i = 0; i < TTL_LIMIT; ++i) {
        for (int j = 0; j < PACKETS_TO_SEND; ++j) {
            init_packet_data(&pds[i][j]);
        }
    }
}

void print_timespec(struct timespec* ta)
{
    printf("timespec: %ld, %ld", ta->tv_sec, ta->tv_nsec);
}

void print_dt(struct timespec* ta, struct timespec* tb)
{
    struct timespec dt;
    dt.tv_sec = tb->tv_sec - ta->tv_sec;
    dt.tv_nsec = tb->tv_nsec - ta->tv_nsec;
    print_timespec(&dt);
}

double delta_t_ms(struct timespec* ta, struct timespec* tb)
{
    return (tb->tv_sec - ta->tv_sec) * 1000.0 + ((double)(tb->tv_nsec - ta->tv_nsec) / 1000000.0);
}

void send_icmp(int sd, int pid, struct icmphdr* header, int seq, struct sockaddr_in* recipient)
{
    init_icmp_hdr_echo(header, pid, seq);
    ssize_t bytes_sent = sendto(
            sd,
            header,
            sizeof(header),
            0,
            (struct sockaddr*)recipient,
            sizeof(*recipient)
            );
    if (bytes_sent < 0) {
        printf("Couldn't send packet: %s  \n",strerror(errno));
    }
}

void send_icmps(int sd, int pid, int ttl, struct icmphdr* header,
                struct sockaddr_in* recipient,
                packet_data pds[TTL_LIMIT + 1][PACKETS_TO_SEND + 1])
{
    int seq = 1;
    setsockopt(sd, IPPROTO_IP, IP_TTL, &ttl, sizeof(int16_t));
    for (int i = 0; i < PACKETS_TO_SEND; ++i) {
        timespec_get(&pds[ttl][i].send_time, TIME_UTC);
        pds[ttl][i].seq = seq;
        pds[ttl][i].checksum = header->checksum;
        send_icmp(sd, pid, header, seq, recipient);
        ++seq;
    }
}


int main(int argc, char** argv)
{
    if (argc < 2) {
        fprintf(stderr, "Pass IP of destination host as argument:\n\t./tc x.y.z.w\n");
        exit(-1);
    }
    int sd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    ssize_t packet_len = -1;
    uint8_t buffer[IP_MAXPACKET+1];
    struct icmphdr* header = (struct icmphdr*)malloc(sizeof(struct icmphdr));
    int pid = getpid();

    struct sockaddr_in* recipient = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
    memset(recipient, 0, sizeof(struct sockaddr_in));
    recipient->sin_family = AF_INET;
    inet_pton(AF_INET, argv[1], &recipient->sin_addr);

    packet_data pds[TTL_LIMIT + 1][PACKETS_TO_SEND + 1];
    init_packets_data(pds);
    int16_t ttl = 1;
    struct timespec tb;
    bool reached = false;
    while (!reached && ttl < TTL_LIMIT) {
        send_icmps(sd, pid, ttl, header, recipient, pds);
        double time_spent = 0.0;
        struct timespec tc;
        timespec_get(&tc, TIME_UTC);
        int received_here = 0;
        while (received_here < PACKETS_TO_SEND && time_spent < TIMEOUT) {
            char sender_ip_str[20];
            timespec_get(&tb, TIME_UTC);
            if (get_packet_from_socket(sd, buffer, &packet_len, sender_ip_str, sizeof(sender_ip_str)) > 0) {
                struct icmphdr* ohdr = (struct icmphdr*)(buffer+20);
                struct icmphdr* hdr = (struct icmphdr*)(buffer+48);
                if (ohdr->type == ICMP_TIME_EXCEEDED
                    && ohdr->code == ICMP_EXC_TTL) {
                    int j = 0;
                    for (; j < PACKETS_TO_SEND && pds[ttl][j].seq != ntohs(hdr->un.echo.sequence); ++j);
                    if (pds[ttl][j].seq == ntohs(hdr->un.echo.sequence)
                     && pid == ntohs(hdr->un.echo.id)) {
                        pds[ttl][j].recv_time = tb;
                        pds[ttl][j].recv = true;
                        ++received_here;
                        strncpy(pds[ttl][j].src_ip_str, sender_ip_str, 18);
                        pds[ttl][j].src_ip_str[19] = '\0';
                    }
                } else if (ohdr->type == ICMP_ECHOREPLY) {
                    reached = true;
                    for (int j = 0; j < PACKETS_TO_SEND; ++j) {
                        pds[ttl][j].recv_time = tb;
                        pds[ttl][j].recv = true;
                        strncpy(pds[ttl][j].src_ip_str, sender_ip_str, 18);
                        pds[ttl][j].src_ip_str[19] = '\0';
                    }
                }
            }
            time_spent += (tb.tv_sec - tc.tv_sec) * 1000.0 + (double)(tb.tv_nsec - tc.tv_nsec) / 1000000.0;
            tc = tb;
        }
        int received = 0;
        double total_time = 0.0;
        for (int i = 0; i < PACKETS_TO_SEND; ++i) {
            if (pds[ttl][i].recv) {
                ++received;
                total_time += delta_t_ms(&pds[ttl][i].send_time, &pds[ttl][i].recv_time);
            }
        }
        if (received > 0) {
            printf("%d. %s ", ttl, pds[ttl][0].src_ip_str);
            if (received < PACKETS_TO_SEND) {
                printf("???\n");
            } else {
                printf("%.2lfms\n", total_time / received);
            }
        } else {
            printf("%d. *\n", ttl);
        }
        ++ttl;
    }
    return 0;
}
