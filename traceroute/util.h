#ifndef UTIL_H
#define UTIL_H

#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/ip_icmp.h>

void print_bytes(uint8_t* buffer, int begin, int len);
int get_packet_from_socket(int sd, uint8_t* buffer, ssize_t* packet_len,
                           char* sender_ip_str, int sender_ip_str_len);
void print_packet(uint8_t* buffer, ssize_t packet_len);
void print_icmphdr(struct icmphdr* hdr);
uint16_t compute_icmp_checksum(const void *buffer, int length);
void init_icmp_hdr_echo(struct icmphdr* hdr, int16_t id, int16_t seq);

#endif /* UTIL_H */
