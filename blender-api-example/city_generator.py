import bpy
import bmesh

import math
import random

import numpy as np


def clear_scene():
    bpy.ops.object.select_all(action='SELECT')
    bpy.ops.object.delete()


def add_camera():
    bpy.ops.object.camera_add(
        view_align=True,
        location=[56,-3,38],
        rotation=[1.05,-0.05,1.5])


def add_light():
    scene = bpy.context.scene
    lamp_data = bpy.data.lamps.new(
        name="New Lamp", type='AREA')
    lamp_object = bpy.data.objects.new(
        name="New Lamp", object_data=lamp_data)
    scene.objects.link(lamp_object)
    lamp_object.location = (35.0, 20.0, 30.0)
    lamp_object.select = True
    scene.objects.active = lamp_object


def add_ground(location=(0, 0, 0), radius=10.0):
    bpy.ops.mesh.primitive_plane_add(location=location, radius=radius)


def add_cube(location=(0, 0, 0)):
    new_cube = bpy.ops.mesh.primitive_cube_add(location=location)


def randomize_heights(n):
    buildings = [ob for ob in bpy.context.scene.objects if ob.type == 'MESH' and ob.name.startswith("Cube")]

    for i in range(n):
        ob = random.choice(buildings)

        new_height = max(1, np.floor(np.random.normal(1, 1) + 2))
        ob.dimensions[2] = new_height
        ob.location[2] = new_height / 2


def main():
    clear_scene()
    add_camera()
    add_light()
    add_ground(radius=20.0)

    building_probability = 0.9

    for x in range(-5, 6):
        for y in range(-5, 6):
            if np.random.uniform(0.0, 1.0) < building_probability:
                add_cube((3 * x, 3 * y, 0.5))

    randomize_heights(20)


if __name__ == '__main__':
    main()
