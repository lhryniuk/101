package main

import (
	"log"

	pb "github.com/hryniuk/is/proto"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

const (
	address = "localhost:50051"
)

func loginUser(userId string, client pb.IsClient) {
	r, err := client.StatusChange(context.Background(), &pb.StatusChangeRequest{UserId: userId, Status: "Online"})
	if err != nil {
		log.Fatalf("could not change status: %v", err)
	}
	log.Printf("Response: %s", r.Reason)
}

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()

	c := pb.NewIsClient(conn)

	loginUser("1", c)
	loginUser("2", c)
	loginUser("3", c)
	loginUser("4", c)
}
