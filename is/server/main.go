package main

import (
	"log"
	"net"

	pb "github.com/hryniuk/is/proto"
	"github.com/mediocregopher/radix.v2/redis"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

type server struct {
	redisClient *redis.Client
}

func (s *server) StatusChange(ctx context.Context, in *pb.StatusChangeRequest) (*pb.StatusChangeResponse, error) {
	if in.Status == "Online" {
		r := s.redisClient.Cmd("SADD", "musers", in.UserId)
		if r.Err != nil {
			log.Fatalf("cannot add user to redis set %s", in.UserId)
		}
	} else {
		s.redisClient.Cmd("SREM", "musers", in.UserId)
	}
	return &pb.StatusChangeResponse{Code: 0, Reason: "Status changed"}, nil
}

const (
	port         = ":50051"
	redisAddress = "localhost:6379"
)

func main() {
	redisClient, err := redis.Dial("tcp", redisAddress)
	if err != nil {
		log.Fatalf("failed to connect to redis on %s", redisAddress)
	}

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	grpcServer := grpc.NewServer()
	pb.RegisterIsServer(grpcServer, &server{redisClient})
	grpcServer.Serve(lis)
}
