import Data.Char

let2int :: Char -> Int
let2int c
 | isLower c = ord c - ord 'a'
 | otherwise = ord c - ord 'A'

int2let :: Int -> Bool -> Char
int2let n b
 | b = chr (ord 'a' + n)
 | otherwise = chr(ord 'A' + n) 

shift :: Int -> Char -> Char
shift n c
 | isLower c = int2let ((let2int c + n) `mod` 26) True
 | isUpper c = int2let ((let2int c + n) `mod` 26) False
 | otherwise = c

encode :: Int -> String -> String
encode n xs = [shift n x | x <- xs]
