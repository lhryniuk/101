
generator :: Integer -> Integer -> Integer -> [Integer]
generator a b s = [a + s * n * signum(b-a) | n <- [0..((abs (b - a)) `div` s)]]
