likes :: [String] -> String
likes a
  | (length a) == 1 = head a ++ " likes this"
  | (length a) == 2 = head a ++ " and " ++ head (drop 1 a) ++ " like this"
  | (length a) == 3 = head a ++ ", " ++ head (drop 1 a) ++ " and " ++ head (drop 2 a) ++ " like this"
  | (length a) > 3 = head a ++ ", " ++ head (drop 1 a) ++
    " and " ++ show(length (drop 2 a)) ++ " others like this"
