module Reverse where
import Prelude hiding (reverse)

-- | Reverse a list
reverse :: [a] -> [a]
reverse [] = []
reverse xs = reverse (tail xs) ++ [head xs]
