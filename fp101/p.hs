module Parsing where


import Data.Char
import Control.Monad

-- infixr 5 +++

-- The monad of parsers
--------------------

newtype Parser a              =  P (String -> [(a,String)])

failure                       :: Parser a
failure                       =  mzero

item                          :: Parser Char
item                          =  P (\inp -> case inp of
                                               []     -> []
                                               (x:xs) -> [(x,xs)])

parse                         :: Parser a -> String -> [(a,String)]
parse (P p) inp               =  p inp


-- instance Monad Parser where
--    return v                   =  P (\inp -> [(v,inp)])
--    p >>= f                    =  error "You must implement (>>=)"
--
-- instance MonadPlus Parser where
--    mzero                      =  P (\inp -> [])
--    p `mplus` q                =  P (\inp -> case parse p inp of
--                                                []        -> parse q inp
--                                                [(v,out)] -> [(v,out)])
