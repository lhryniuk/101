fn :: Int -> Int -> Int -> Int -> Int
fn a b c d = (a * a + b * b) * (c * c + d * d)

isSumOfSquares :: Int -> Int -> Int -> Int -> Int -> Bool
isSumOfSquares n a b c d = (n == a * b + c * d)

isComposition :: Int -> Int -> Int -> Int -> Int -> Bool
isComposition n a b c d =
    if
    (isSumOfSquares n a b c d) ||
    (isSumOfSquares n a c b d) ||
    (isSumOfSquares n a d b c)
        then True else
            if n == 0
                then True else
                    (a /= 0 && isComposition (n + a) 0 b c d) ||
                    (b /= 0 && isComposition (n + b) a 0 c d) ||
                    (c /= 0 && isComposition (n + c) a b 0 d) ||
                    (d /= 0 && isComposition (n + d) a b c 0) ||
                    (a /= 0 && isComposition (n - a) 0 b c d) ||
                    (b /= 0 && isComposition (n - b) a 0 c d) ||
                    (c /= 0 && isComposition (n - c) a b 0 d) ||
                    (d /= 0 && isComposition (n - d) a b c 0) ||
                    (a /= 0 && n `mod` a == 0 && isComposition (n `div` a) 0 b c d) ||
                    (b /= 0 && n `mod` b == 0 && isComposition (n `div` b) a 0 c d) ||
                    (c /= 0 && n `mod` c == 0 && isComposition (n `div` c) a b 0 d) ||
                    (d /= 0 && n `mod` d == 0 && isComposition (n `div` d) a b c 0)


prod2Sum :: Int -> Int -> Int -> Int -> [(Int, Int)]
prod2Sum a b c d = 
    [(x, y) | x <- [1..l],
              y <- [x..l],
              x * x + y * y == n,
              (isComposition x a b c d), (isComposition y a b c d)]
        where n = fn a b c d
              l = (ceiling (sqrt (fromIntegral n)))

d a = a * 2

readAnInt :: IO Int
readAnInt = readLn

main :: IO ()
main = do
    a <- readAnInt
    b <- readAnInt
    c <- readAnInt
    d <- readAnInt
    putStrLn (show (prod2Sum a b c d))
    -- k <- readAnInt
    -- putStrLn (show (isComposition k a b c d))
    -- putStrLn (show (areSame (a, b) (c, d)))
    -- putStrLn (show (f a b c d))
