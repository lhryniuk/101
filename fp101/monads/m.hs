
import Control.Monad

newtype Parser a              =  P (String -> [String])

instance Monad Parser where
   return v                   =  P (\inp -> [inp])
   p >>= f                    =  error "You must implement (>>=)"

instance MonadPlus Parser where
   mzero                      =  P (\inp -> [])
   p `mplus` q                =  P (\inp -> [inp]) 

sq :: Integer -> Integer
sq x = x * x

main = do {
        return $sq 10;
    }

