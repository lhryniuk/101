import Data.Char


xo :: String -> Bool
xo str = count 'x' str == count 'o' str
    where count c s = length $ (filter (== c) . map toLower) s
