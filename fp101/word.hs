
import Data.List
import Data.List.Split

convert :: String -> String
convert str = head str : show (length str - 2) ++ [last str]

abbreviate :: String -> String
abbreviate str
  | length str <= 4 = str
  | otherwise = intercalate "-" $ map convert $ splitOn "-" str
