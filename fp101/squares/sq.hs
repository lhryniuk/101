

squares :: Integer -> Int -> [Integer]
squares x n = [x ^ (2 ^ k) | k <- [0..n-1]]
