
mostFrequentItemCount :: [Int] -> Int
mostFrequentItemCount xs = maximum [length(filter (\a -> a == x) xs) | x <- xs]
