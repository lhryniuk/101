#include <assert.h>
#include <errno.h>
#include <netdb.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>

int create_tcp_socket(struct addrinfo** res, const char* port)
{
    int sd = 0;
    struct addrinfo hints;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    int rc = getaddrinfo("localhost", port, &hints, res);

    if (rc != 0) {
        perror(NULL);
        exit(1);
    }

    sd = socket((*res)->ai_family, (*res)->ai_socktype, (*res)->ai_protocol);

    if (sd == -1) {
        perror(NULL);
        exit(1);
    }

    return sd;
}

void open_connection(int sockfd, struct addrinfo* res)
{
    int rc = connect(sockfd, res->ai_addr, res->ai_addrlen);
    if (rc == -1) {
        perror(NULL);
        exit(1);
    }

    return;
}

int main(int argc, char** argv)
{
    char port[] = "8765";
    if (argc >= 2) {
        strncpy(port, argv[1], 4);
    }
    struct addrinfo *res = NULL;
    int socket_fd = create_tcp_socket(&res, port);

    open_connection(socket_fd, res);

    char *data = (char*)malloc(100);

    while (true) {
        scanf("%s", data);
        int len = strlen(data);
        data[len] = '\n';
        ++len;


        int rc = send(socket_fd, data, len, 0);
        if (rc == -1) {
            perror(NULL);
            exit(1);
        }
    }
    free(data);

    return 0;
}
