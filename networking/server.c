#include <arpa/inet.h>
#include <assert.h>
#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

const int BUFLEN = 512;
const int PORT = 9930;

void usage(const char* program_name)
{
    printf("Usage:\n");
    printf("\t%s PORT\n", program_name);
    printf("\nwill start server listening on port PORT.\n");
}

void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int main(int argc, char** argv)
{
    if (argc != 2) {
        usage(argv[0]);
        exit(1);
    }

    setbuf(stdout, NULL);

    const int port_len = 10;
    char port[10];
    strncpy(port, argv[1], port_len);

    struct addrinfo* res = NULL;
    struct addrinfo hints;

    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    getaddrinfo(NULL, port, &hints, &res);

    int sd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
    if (sd == -1) {
        perror(NULL);
        exit(1);
    }

    if (bind(sd, res->ai_addr, res->ai_addrlen) == -1) {
        perror(NULL);
        exit(-1);
    }

    if (listen(sd, 2) == -1) {
        perror(NULL);
        exit(1);
    }

    struct sockaddr_storage their_addr;
    socklen_t addr_size = sizeof their_addr;

    while (1) {
        int new_sockfd = accept(sd, (struct sockaddr*)&their_addr, &addr_size);

        if (new_sockfd == -1) {
            perror("accept");
            continue;
        }

        char s[INET6_ADDRSTRLEN];
        inet_ntop(their_addr.ss_family,
                get_in_addr((struct sockaddr*)&their_addr),
                s, sizeof s);
        printf("server: got connection from %s\n", s);

        pid_t pid = fork();
        if (pid == 0) {
            close(sd);
            const char* msg = "received";
            char* received = (char*)malloc(100);
            int lenght = -1;
            while ((lenght = recv(new_sockfd, (void*)received, 10, 0)) != -1) {
                if (send(new_sockfd, msg, strlen(msg), 0) == -1) {
                    perror("error\n");
                }

                if (lenght > 0) {
                    printf("received:");
                    for (int i = 0; i < lenght; ++i) {
                        printf("%c", received[i]);
                    }
                }
            }
        }
        close(new_sockfd);
    }

    return 0;
}
