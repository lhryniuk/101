
const double EPS = 10e-9;

struct POINT // wspolrzedne calkowitoliczbowe
{
    int x, y;
    POINT(int x = 0, int y = 0) : x(x), y(y) { }
    bool operator==(POINT &a)
    {
        return a.x == x && a.y == y;
    }
};

inline bool IsZero(double x)
{
    return x >= -EPS && x <= EPS;
}

struct POINTD // wspolrzedne rzeczywiste
{
    double x, y;
    POINTD(double x = 0, double y = 0) : x(x), y(y) { }
    POINTD(const POINT &a) : x(a.x), y(a.y) { }
    bool operator==(POINT &a)
    {
        return IsZero(a.x - x) && IsZero(a.y - y);
    }
};

