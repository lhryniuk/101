#include <iostream>
#include <vector>
#include <set>
#include <algorithm>
using namespace std;

typedef long long LL;
set<int> in_seq; // number present in sequence
vector<LL> EKG; // numbers of ekg sequence

LL gcd(LL a, LL b)
{
  while (b) {
    swap(a%=b, b);
  }
  return a;
}

void generate_ekg(int n)
{
  for (int i = 2; i < n; ++i) {
    int k = 1;
    while ((in_seq.find(k) != in_seq.end()) || gcd(k, EKG[i]) == 1) {
      ++k;
    }
    EKG.push_back(k);
    in_seq.insert(k);
  }
}

int main(void)
{
  /*
   * a(1) = 1
   * a(2) = 2
   * a(n+1) - the smallest positive integer that gcd(a(n), a(n+1)) > 1
   */
  EKG.push_back(-1); // 0 element
  EKG.push_back(1);
  EKG.push_back(2);
  in_seq.insert(1);
  in_seq.insert(2);
  generate_ekg(200);
  for (int i = 1; i <= 40; ++i) {
    std::cout << EKG[i] << ' ';
  }
  std::cout << '\n';
  return 0;
}
