#include <fstream>

int main(void)
{
  std::ofstream out("potegi.txt");
  out << "{ ";
  unsigned long long pot = 1;
  for (int i = 0; i < 64; ++i) {
    out << pot << ",\n";
    pot <<= 1;
  }
  out << " };\n";
}
