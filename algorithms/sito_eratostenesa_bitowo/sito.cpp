#include <iostream>
#include <cassert>
#include <vector>

typedef unsigned long long ULL;

ULL maski[] = 
{ 1,
  2,
  4,
  8,
  16,
  32,
  64,
  128,
  256,
  512,
  1024,
  2048,
  4096,
  8192,
  16384,
  32768,
  65536,
  131072,
  262144,
  524288,
  1048576,
  2097152,
  4194304,
  8388608,
  16777216,
  33554432,
  67108864,
  134217728,
  268435456,
  536870912,
  1073741824,
  2147483648ULL,
  4294967296ULL,
  8589934592ULL,
  17179869184ULL,
  34359738368ULL,
  68719476736ULL,
  137438953472ULL,
  274877906944ULL,
  549755813888ULL,
  1099511627776ULL,
  2199023255552ULL,
  4398046511104ULL,
  8796093022208ULL,
  17592186044416ULL,
  35184372088832ULL,
  70368744177664ULL,
  140737488355328ULL,
  281474976710656ULL,
  562949953421312ULL,
  1125899906842624ULL,
  2251799813685248ULL,
  4503599627370496ULL,
  9007199254740992ULL,
  18014398509481984ULL,
  36028797018963968ULL,
  72057594037927936ULL,
  144115188075855872ULL,
  288230376151711744ULL,
  576460752303423488ULL,
  1152921504606846976ULL,
  2305843009213693952ULL,
  4611686018427387904ULL,
  9223372036854775808ULL };

ULL mask64 = 63;

bool co_tam(const std::vector<ULL>& V, ULL i)
{
  ULL j = i >> 6; 
  ULL r = i & mask64;
  return (V[j] & maski[r]);
}

void falsuj(std::vector<ULL>& V, ULL i)
{
  ULL j = i >> 6; 
  ULL r = i & mask64;
  V[j] &= (~maski[r]);
}

std::vector<ULL> sito_erat(ULL N=1000000000)
{
  std::vector<ULL> sito(15625000, 0xAAAAAAAAAAAAAAAA);
  falsuj(sito, 0);
  falsuj(sito, 1);
  sito[0] |= maski[2];
 /* przechodzimy jedynie do sqrt(N), poniewaz jezeli 
  * jakas liczba ma wiekszy dzielnik, to zostala juz
  * skreslona */
  for (unsigned long long i = 3; i * i <= N; ++i) {
    if (co_tam(sito, i)) {
      /*  wykreslamy wielokrotnosci od i * i, bo poprzednie 
       *  wielokrotnosci zostaly juz skreslone */
      for(unsigned long long j = i; i * j <= N; ++j) {
        falsuj(sito, i * j);
      }
    }
  }
  return sito;
}

int main()
{
  std::vector<ULL> P = sito_erat();
  std::vector<ULL> sito(15625000, 0xFFFFFFFFFFFFFFFF);
  if (co_tam(P, 127)) {
    std::cout << "dziala chyba" << std::endl;
  }
}
