#include <iostream>
#include <string>
using namespace std;

#define SIZE(x) int((x).size())

int szukaj(string t, string w)
{
    int i, j, x, y;
    i = j = 0;
    x = SIZE(t);
    y = SIZE(w);
    while((i < x) && (j < y))
    {
        if(t[i] != w[j])
            j = -1;
        ++i;
        ++j;
    }
    return (j == y) ? (i - y) : -1;
}

int main()
{
    string a = "abrakadabra";
    string b = "rak";
    cout << szukaj(a, b) << endl;
}
