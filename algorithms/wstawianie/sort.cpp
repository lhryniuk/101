#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

const int ROZ = 10;
int tab[ROZ];

void losuj(int tab[], int r)
{
    for(int i = 0; i < r; ++i) tab[i] = rand() % (r * 2);
}

void insert_sort(int tab[], int r)
{
    for(int i = 1; i < r; ++i)
    {
        int j = i;
        int tmp = tab[j];
        while((j > 0) && (tab[j-1] > tmp))
        {
            tab[j] = tab[j-1];
            --j;
        }
        tab[j] = tmp;
    }
}

int main()
{
    losuj(tab, ROZ);
    for(int i = 0; i < ROZ; ++i) cout << tab[i] << ' ';
    cout << endl;
    insert_sort(tab, ROZ);
    for(int i = 0; i < ROZ; ++i) cout << tab[i] << ' ';
    cout << endl;
}
