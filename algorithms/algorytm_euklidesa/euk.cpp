#include <cstdio>
#include <algorithm>
typedef long long LL;
using namespace std;

LL nwd(LL a, LL b)
{
    while(b) swap(a %= b, b);
    return a;
}

int main()
{
    LL a, b;
    scanf("%lld %lld", &a, &b);
    printf("%lld\n", nwd(a, b));
}
