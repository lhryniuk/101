#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

const int ROZ = 5;
int tab[ROZ];

void losuj(int tab[], int r)
{
        srand(time(NULL));
        for(int i = 0; i < r; ++i) tab[i] = rand() % (r * 2);
}

void wypisz(int tab[], int r)
{
        for(int i = 0; i < r; ++i)
                cout << tab[i] << ' ';
        cout << endl;
}

void quicksort(int tab[], int left, int right)
{
        int m = left;
        if (left < right) {
                for (int i = left + 1; i <= right; ++i) {
                        if (tab[i] < tab[left]) {
                                swap(tab[++m], tab[i]);
                        }
                }
                swap(tab[left], tab[m]);
                quicksort(tab, left, m-1);
                quicksort(tab, m+1, right);
        }
}

int main()
{
        losuj(tab, ROZ);
        wypisz(tab, ROZ);
        quicksort(tab, 0, ROZ-1);
        wypisz(tab, ROZ);
}
