/* kolorowanie grafu
 * liczba chromatyczna */
#include <iostream>
#include <vector>
#include <utility>
#include <algorithm>
using namespace std;

#define PB push_back
#define SIZE(c) int((c).size())
#define REP(x, n) for (int x = 0; x < n; ++x)
#define FOR(x, b, n) for (int x = b; x <= n; ++x)

const int SIZE = 1001;
vector<int> G[SIZE];
pair<int, int> st[SIZE]; // stopnie wierzchołków
bool kol[SIZE]; // kolory przy aktualnym wierzcholku
int num[SIZE];  // kolor danego wierzcholka

bool cmp(pair<int, int> a, pair<int, int> b)
{
    if (b.second > a.second) return true;
    if (b.second < a.second) return false;
    return (a.first < b.first);
}

inline void zero()
{
    REP(i, SIZE) kol[i] = false;
}

void init()
{
    REP(i, SIZE) {
        st[i].first = i;
    }
}

int main()
{
    ios_base::sync_with_stdio(0);
    init();
    int l = 0; // liczba chromatyczna
    int k = 1; // kolor do pomalowania
    int n, m;
    int a, b;
    cin >> n >> m; // liczba_wierzcholkow liczba_krawedzi
    REP(i, m) {
        cin >> a >> b;
        G[a].PB(b);
        G[b].PB(a);
        ++st[a].second;
        ++st[b].second;
    }
    stable_sort(st, st+SIZE, cmp);  
    num[st[0].first] = 1;
    FOR(i, 1, SIZE-1) {
        int v = st[i].first;
        int s = SIZE(G[v]);
        REP(i, s) {
            kol[num[G[v]]] = true;
        }

    }
    return 0;
}
