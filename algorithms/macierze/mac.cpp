#include <iostream>
using namespace std;

struct macierz
{
        int t[1000][1000];
        int m;
        int n;
        macierz& operator=(macierz& a)
        {
                m = a.m;
                n = a.n;
                for(int i = 1; i <= m; ++i)
                {
                        for(int j = 1; j <= n; ++j)
                        {
                                a.t[i][j] = t[i][j];
                        }
                }
                return a;
        }
        macierz& operator+(macierz& a)
        {
                if((m != a.m) && (n != a.n)) return *this;;
                for(int i = 1; i <= m; ++i)
                {
                        for(int j = 1; j <= n; ++j)
                        {
                                t[i][j] += a.t[i][j];
                        }
                }
                return *this;
        }
        macierz& operator*(const int k)
        {
                for(int i = 1; i <= m; ++i)
                {
                        for(int j = 1; j <= n; ++j)
                        {
                                t[i][j] *= k;
                        }
                }
                return *this;
        }
};

macierz mnoz(macierz a, macierz b)
{
        macierz g;
        g.m = a.m;
        g.n = b.n;
        for(int i = 1; i <= g.m; ++i)
        {
                for(int j = 1; j <= g.n; ++j)
                {
                        for(int x = 1; x <= a.n; ++x)
                        {
                                g.t[i][j] += a.t[i][x] + b.t[x][j];
                        }
                }
        }
        return g;
}

int main()
{
        int m, n;
        macierz a;
        macierz b;
        cout << "Macierz a" << endl;
        cin >> m >> n;
        a.m = m;
        a.n = n;
        for(int i = 1; i <= a.m; ++i)
        {
                for(int j = 1; j <= a.n; ++j)
                {
                        cin >> a.t[i][j];
                }
        }
        cout << "Macierz b" << endl;
        cin >> m >> n;
        b.m = m;
        b.n = n;
        for(int i = 1; i <= b.m; ++i)
        {
                for(int j = 1; j <= b.n; ++j)
                {
                        cin >> b.t[i][j];
                }
        }
        macierz tmp = mnoz(a, b);
        for(int i = 1; i <= a.m; ++i)
        {
                for(int j = 1; j <= a.n; ++j)
                {
                        cout << a.t[i][j] << ' ';
                }
                cout << endl;
        }
        cout << endl;
        return 0;
}
