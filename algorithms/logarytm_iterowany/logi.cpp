#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>
#include <queue>
#include <list>
#include <set>
using namespace std;

typedef long long LL;
typedef unsigned long long ULL;
typedef vector<int> VI;
typedef vector<string> VS;

#define FOR(x, b, e) for (int x = b; x <= (e); ++x)
#define FORD(x, b, e) for (int x = b; x >= (e); --x)
#define REP(x, n) for (int x = 0; x < n; ++x)
#define ALL(c) (c).begin(), (c).end()
#define SIZE(c) ((int)(c).size())
#define PB push_back
const int INF = 100000001;
const double EPS = 10e-9;

double logi(double n)
{
        if (n <= 1) return 0;
        return 1 + logi(log(n));
}

int main()
{
        ios_base::sync_with_stdio(0);
        double x;
        while (cin >> x && x) {
                cout << logi(x) << endl;
        }
        return 0;
}

