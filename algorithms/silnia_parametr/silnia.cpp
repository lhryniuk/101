#include <iostream>
using namespace std;
typedef unsigned long long ull;

ull silnia(ull x, ull tmp=1)
{
    if(x == 0) return tmp;
    return silnia(x-1, x * tmp);
}

int main()
{
    ull n;
    cin >> n;
    cout << silnia(n) << endl;
}
