#include <iostream>
using namespace std;

const int ROZ = 1000010;
bool p[ROZ];

void init()
{
        for (int i = 2; i < ROZ; ++i) {
                p[i] = true; // na poczatku wszystkie sa pierwsze
        }
        for (int i = 2; i * i < ROZ; ++i) { // wykreslamy wielokrotnosci
                if (p[i]) {
                        for(int j = i; i * j < ROZ; ++j) {
                                p[i*j] = false;
                        }
                }
        }
}

int main()
{
        ios_base::sync_with_stdio(0);
        init();
        int a;
        while(cin >> a && a != 0)
        {
                if(p[a]) cout << "Pierwsza." << endl;
                else cout << "Złożona." << endl;
        }
}
