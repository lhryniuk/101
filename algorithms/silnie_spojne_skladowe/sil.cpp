#include <iostream>
#include <vector>
using namespace std;

#define PB push_back
#define SIZE(c) int((c).size())

const int size = 100;
vector<int> G[size];
vector<int> T[size];
bool vis[size];
int dist[size];
int czasy[size];
int tmp = 0;
int sss[size];

void init()
{
        for(int i = 0; i < size; ++i)
                vis[i] = false;
}

void dfs(int u)
{
        if(vis[u]) return;
        vis[u] = true;
        int x = SIZE(G[u]);
        for(int i = 0; i < x; ++i)
                dfs(G[u][i]);
        ++tmp;
        czasy[tmp] = u;
}

void dfs_rev(int u, int s)
{
        if(vis[u]) return;
        sss[u] = s;
        vis[u] = true;
        int x = SIZE(T[u]);
        for(int i = 0; i < x; ++i)
                dfs_rev(T[u][i], s);
}

void Trans()
{
        for(int i = 0; i < size; ++i)
        {
                int x = SIZE(G[i]);
                for(int j = 0; j < x; ++j)
                {
                        T[G[i][j]].PB(i);
                }
        }
}

int main()
{
        ios_base::sync_with_stdio(0);
        G[1].PB(2);
        G[2].PB(1);
        G[2].PB(3);
        G[4].PB(4);
        /*G[1].PB(2);
          G[2].PB(3);
          G[3].PB(4);
          G[4].PB(5);
          G[5].PB(6);
          G[6].PB(5);
          G[4].PB(2);
          G[4].PB(7);
          G[7].PB(2);*/
        /*for(int i = 0; i < size; ++i)
          {
          int r = SIZE(G[i]);
          if(r > 0)
          {
          cout << i << ": ";
          for(int j = 0; j < r; ++j)
          {
          cout << G[i][j] << ' ';
          }
          cout << endl;
          }
          }*/
        dfs(1);
        Trans();
        init();
        /*
           for(int i = 0; i < size; ++i)
           {
           if(czasy[i])
           {
           cout << "Czas przetworzenia wierzcholka " << i << " = " << czasy[i] << "." << endl;
           }
           }
           */
        int spoj = 1;
        for(int i = size - 1; i >= 0; --i)
        {
                if(czasy[i] && !vis[czasy[i]])
                {
                        dfs_rev(czasy[i], spoj);
                        ++spoj;
                }
        }
        for(int i = 0 ; i < size; ++i)
        {
                if(sss[i])
                        cout << "Wierzcholek " << i << " nalezy do " << sss[i] << " silnie spojnej skladowej." << endl;
        }
}
