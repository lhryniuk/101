#include <iostream>
#include <sstream>
#include <vector>
using namespace std;

int exponent = 6;

unsigned long long s2ul(string s)
{
  unsigned long long c = 0;
  stringstream ss;
  ss << s;
  ss >> c;
  return c;
}

vector<unsigned long long> parts(string s)
{
  vector<unsigned long long> p;
  int i;
  for (i = s.size(); i >= exponent; i -= exponent) {
    string part = s.substr(i-exponent, exponent);
    p.push_back(s2ul(part));
  }
  p.push_back(s2ul(s.substr(0, i)));
  return p;
}

string ul2s(unsigned long long k)
{
  stringstream ss;
  string a;
  ss << k;
  ss >> a;
  if (int(a.size()) < exponent) {
    a = string(exponent - a.size(), '0') + a;
  }
  return a;
}

void print_result(vector<unsigned long long> result)
{
  for (int i = 0; i < int(result.size()) - 1; ++i) {
    result[i+1] += result[i] / 1000000;
    result[i] %= 1000000;
  }
  bool out = false;
  int i = result.size();
  while (!result[--i]);
  for (; i >= 0; --i) {
    //std::cout << i << " = ";
    if (out) {
      std::cout << ul2s(result[i]);
    } else {
      std::cout << result[i];
      out = true;
    }
    //std::cout << '\n';
  }
  std::cout << '\n';
}

void mult(std::string a, std::string b)
{
  a = string(max(0, int(b.size() - a.size())), '0') + a;
  b = string(max(0, int(a.size() - b.size())), '0') + b;
  if (int(a.size()) % 2) {
    a = '0' + a;
    b = '0' + b;
  }
  vector<unsigned long long> a_parts = parts(a);
  vector<unsigned long long> b_parts = parts(b);
  //cout << "czesci a:\n";
  //for (int i = 0; i < int(a_parts.size()); ++i) {
    //cout << a_parts[i] << ' ';
  //}
  //cout << '\n';
  //cout << "czesci b:\n";
  //for (int i = 0; i < int(b_parts.size()); ++i) {
    //cout << b_parts[i] << ' ';
  //}
  //cout << '\n';
  vector<unsigned long long> result(a_parts.size() + b_parts.size() + 1);
  for (int i = 0; i < int(a_parts.size()); ++i) {
    for (int j = 0; j < int(b_parts.size()); ++j) {
      //cout << "mnoze i: " << i << " = " << a_parts[i] 
          //<< "\noraz j: " << j << " = " << b_parts[j]
          //<< "\ni umieszczam w " << i + j << endl;
      result[i+j] += a_parts[i] * b_parts[j];
      //cout << "wynik = " << result[i+j] << endl;
    }
  }
  print_result(result);
}

int main(void)
{
  ios_base::sync_with_stdio(0);
  int n;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    string l1, l2;
    cin >> l1 >> l2;
    mult(l1, l2);
  }
  return 0;
}
