#include <iostream>
using namespace std;

#define REP(x, n) for (int x = 0; x < n; ++x)
#define FORD(x, b) for (int x = b; x >= 0; --x)

const int INF = -1;
const int SIZE = 10000;
int wartosc[SIZE];

int main()
{
  ios_base::sync_with_stdio(0);
  REP(i, SIZE) wartosc[i] = INF;
  wartosc[0] = 0;
  int n; // ilosc przedmiotow
  int p; // pojemnosc
  cin >> n >> p;
  REP(i, n) {
    int a, b; // a - cena, b - waga
    cin >> a >> b;
    FORD(j, p - b) { // zaczynamy od miejsca, 
      // w ktorym nie przekroczymy pojemnosci
      if (wartosc[j] != INF) {
        if (wartosc[j + b] < wartosc[j] + a) { /* jezeli po
                                                * dolozeniu aktualnego
                                                * przedmiotu, sytuacja 
                                                * poprawi, zapisujemy */
          wartosc[j + b] = wartosc[j] + a;
        }
      }
    }
  }
  int m = 0;
  REP(i, p+1) {
    m = max(m, wartosc[i]); // szukamy najlepszej sytuacji
  }
  cout << m << endl;
  return 0;
}
