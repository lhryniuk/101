#include <cassert>
#include <iostream>
#include <limits>
#include <memory>
#include <random>
#include <type_traits>
#include <stdexcept>

constexpr int k = 10;


int get_random_priority()
{
    static std::random_device rd{};
    static std::mt19937 gen{rd()};
    static std::uniform_int_distribution<> dis{1, std::numeric_limits<int>::max()};

    return dis(gen);
}


template<
    typename T,
    typename = typename std::enable_if<std::is_arithmetic<T>::value, T>::type
>
struct treap
{
    ~treap()
    {
        delete root;
    }
    struct node
    {
        node(const T value)
            : value_{value}, priority_{get_random_priority()}
        {
        }
        ~node()
        {
            delete left_;
            delete right_;
        }
        node* left_ = nullptr;
        node* right_ = nullptr;
        T value_;
        int priority_;
    };

    void rotate_left(node*& t) const
    {
        node* new_root{t->right_};
        t->right_ = new_root->left_;
        new_root->left_ = t;

        t = new_root;
    }

    void rotate_right(node*& t) const
    {
        node* new_root{t->left_};
        t->left_ = new_root->right_;
        new_root->right_ = t;

        t = new_root;
    }

    bool is_valid()
    {
        is_valid(root);
    }

    bool is_valid(
            const node* const t,
            int parent_priority=std::numeric_limits<T>::max(),
            T min=std::numeric_limits<T>::min(),
            T max=std::numeric_limits<T>::max())
    {
        if (t == nullptr) return true;

        const auto valid = (min <= t->value_ && t->value_ <= max && t->priority_ <= parent_priority)
            && is_valid(t->left_, t->priority_, min, t->value_)
            && is_valid(t->right_, t->priority_, t->value_, max);
        if (not valid) {
            throw std::logic_error{{}};
        }
        return valid;
    }

    void fix(node*& t)
    {
        if (t->left_ && (t->priority_ < t->left_->priority_)) {
            rotate_right(t);
        }
        if (t->right_ && (t->priority_ < t->right_->priority_)) {
            rotate_left(t);
        }
    }

    void insert(T value)
    {
        insert(root, value);
    }

    void insert(node*& t, T value)
    {
        if (t == nullptr) {
            t = new node(value);
            return;
        }

        if (value <= t->value_) {
            insert(t->left_, value);
        } else {
            insert(t->right_, value);
        }
        fix(t);
    }

    void remove(T value)
    {
        remove(root, value);
    }

    void remove(node*& t, T value)
    {
        if (t == nullptr) {
            return;
        }
        if (t->value_ == value) {
            remove(t);
            return;
        }
        if (value <= t->value_) {
            remove(t->left_, value);
        } else {
            remove(t->right_, value);
        }
    }

    void remove(node*& t)
    {
        if (t == nullptr) {
            return;
        }
        if (t->left_ == nullptr && t->right_ == nullptr) {
            delete t;
            t = nullptr;
            return;
        }

        if (t->left_ && !t->right_) {
            rotate_right(t);
            remove(t->right_);
            return;
        }

        if (t->right_ && !t->left_) {
            rotate_left(t);
            remove(t->left_);
            return;
        }

        if (t->left_->priority_ < t->right_->priority_) {
            rotate_left(t);
            remove(t->left_);
        } else {
            rotate_right(t);
            remove(t->right_);
        }
    }

    node* find(T value) const
    {
        return find(root, value);
    }

    node* find(node* t, T value) const
    {
        if (t == nullptr) {
            return nullptr;
        }
        if (t->value_ == value) {
            return t;
        }
        if (t->value_ != value && !(t->left_ || t->right_)) {
            return nullptr;
        }

        if (value <= t->value_) {
            return find(t->left_, value);
        } else {
            return find(t->right_, value);
        }
    }

    node* root = nullptr;
};

void random_test(int n)
{
    auto mv = std::max(n, 100);
    std::random_device rd{};
    std::mt19937 gen{rd()};
    std::uniform_int_distribution<> dis{1, mv};

    std::vector<int> vs;
    treap<int> root;

    try {
        for (int j = 0; j < n; ++j) {
            const auto v = dis(gen);
            if (root.find(v) == nullptr) {
                root.insert(v);
                vs.push_back(v);
            }
            assert(root.find(v));
            root.is_valid();
        }
        for (auto v : vs) {
            root.remove(v);
            assert(!root.find(v));
        }
    }
    catch (std::logic_error& e) {
        std::cout << "FAIL!" << std::endl;
        return;
    }

    std::cout << "OK!" << std::endl;
}

int main(int argc, char** argv)
{
    if (argc > 1) {
        random_test(std::stoi(argv[1]));
    } else {
        random_test(k);
    }
}
