#include <iostream>
using namespace std;

/*
 * pozwalaja na przypisywanie elementom
 * wartosci ze zbioru liczb calkowitych
 * oraz wyznaczanie sumy wartosci
 * w spojnym przedziale elementow
 */
struct licz
{
        int czapa;
        int *tab;
        licz(int r)
        {
                czapa = r / 2;
                tab = new int[r]; 
        
        }
        ~licz()
        {
                delete[] tab;
        }
        // zmienia liczbe liczb od danej wartosci
        // o n - w przypadku ujemnego n mozna zmienic:
        // tab[i] += n;
        // na:
        // tab[i] = max(tab[i] + n, 0);
        void set(int p, int n = 1)
        {
                int i = czapa + p;
                tab[i] += n;
                while (i != 1) {
                        i >>= 1;
                        tab[i] += n;
                }
        }
        int mini()
        {
                int i = 1;
                /*
                 * dopoki jestesmy w przedzialach
                 * przechodzimy do lewego syna, 
                 * a jezeli nie ma w danym przedziale
                 * zadnej liczby do prawego
                 * */
                while (i < czapa) {
                        i <<= 1;
                        if (!tab[i]) {
                                ++i;
                        }
                }
                return (i - czapa);
        }
        int maxi()
        {
                int i = 1;
                /*
                 * analogicznie jak w funkcji mini()
                 * przechodzimy w miare mozliwosci
                 * do prawego syna
                 */
                while (i < czapa) {
                        i <<= 1;
                        ++i;
                        if (!tab[i]) {
                                --i;
                        }
                }
                return (i - czapa);
        }
        /*
         * ilosc liczb wiekszych od a
         * (analogicznie piszemy mniejsze)
         */
        int wieksze(int a)
        {
                int i = czapa + a;
                int l = 0;
                while (i > 1) {
                        if (!(i % 2)) {
                                l += tab[i + 1];
                        }
                        i >>= 1;
                }
                return l;
        }
        int mediana(int k)
        {
                int i = 1;
                while (i < czapa) {
                        if (tab[(i << 1)] >= k) {
                                i <<= 1;
                        } else {
                                k -= tab[(i << 1)];
                                i <<= 1;
                                ++i;
                        }
                }
                return i - czapa;
        }
};

int main()
{
        licz tree(2048);
        tree.set(5);
        tree.set(6);
        cout << tree.mini() << endl;
        tree.set(8);
        cout << tree.mini() << endl;
        cout << tree.wieksze(10) << endl;
        cout << tree.maxi() << endl;
        tree.set(1009);
        cout << tree.maxi() << endl;
        cout << tree.wieksze(5) << endl;
        cout << tree.wieksze(1009) << endl;
        cout << tree.mediana((5 + 1) / 2) << endl;
        return 0;
}
