#include <iostream>
using namespace std;

struct kop
{
        int size;
        int n;
        int *tab;
        kop(int s=0) : size(s), n(0)
        {
                tab = new int[s+1];
        }
        ~kop()
        {
                delete[] tab;
        }
        void repair(int v)
        {
                if (v > n) return;
                int k = tab[v];
                int tmp_i = v;
                int left = v * 2;
                int right = left + 1;
                if (left <= n && tab[left] > k) {
                        tmp_i = left;
                        k = tab[left];
                }
                if (right <= n && tab[right] > k) {
                        tmp_i = right;
                        k = tab[right];
                }
                if (tmp_i != v) {
                        tab[tmp_i] = tab[v];
                        tab[v] = k;
                        repair(tmp_i);
                }
        }
        void build()
        {
                int k = n / 2;
                for (int i = k; i >= 1; --i) {
                        repair(i);
                }
        }
        void insert(int a)
        {
                ++n;
                tab[n] = a;
                build();
        }
        void remove(int v)
        {
                if (v > n) {
                        return;
                }
                tab[v] = tab[n];
                --n;
                build();
        }
};

void heapsort(int *tab, int n)
{
        kop kopiec(1000);
        for (int i = 0; i < n; ++i) {
                kopiec.insert(tab[i]);
        }
        for (int i = n-1; i >= 0; --i) {
                tab[i] = kopiec.tab[1];
                kopiec.remove(1);
        }
}

int main()
{
        ios_base::sync_with_stdio(0);
        int n;
        cin >> n;
        int *tab = new int[n];
        for (int i = 0; i < n; ++i) {
                cin >> tab[i];
        }
        heapsort(tab, n);
        for (int i = 0; i < n; ++i) {
                cout << tab[i] << ' ';
        }
        cout << endl;
        delete[] tab;
        return 0;
}
