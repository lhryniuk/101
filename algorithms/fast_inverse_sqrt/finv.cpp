#include <iostream>
#include <cmath>
using namespace std;

float FastInvSqrt(float x) {
        float xhalf = 0.5f * x;
        int i = *(int*)&x;         // evil floating point bit level hacking
        i = 0x5f3759df - (i >> 1);  // what the fuck?
        x = *(float*)&i;
        x = x*(1.5f-(xhalf*x*x));
        return x;
}

int main()
{
        ios_base::sync_with_stdio(0);
        double x = 0;
        for (int i = 1; i < 1000000000; ++i) {
                x += FastInvSqrt(i);
        }
        cout << x << endl;
        return 0;
}
