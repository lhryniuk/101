#include <iostream>
#include <cmath>
using namespace std;

int main()
{
        ios_base::sync_with_stdio(0);
        double x = 0;
        for (int i = 1; i < 1000000000; ++i) {
                x += 1.0 / sqrt(double(i));
        }
        cout << x << endl;
        return 0;
}
