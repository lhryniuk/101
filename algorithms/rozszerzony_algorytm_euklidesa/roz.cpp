#include <iostream>
using namespace std;

int gcdw(int a, int b, int& l, int& k)
{
  if (!a) {
    l = 0;
    k = 1;
    return b;
  }
  int d = gcdw(b % a, a, k, l);
  l -= (b / a) * k;
  return d;

}

int main()
{
  int a, b;
  int l, k;
  cin >> a >> b;
  int gcd = gcdw(a, b, l, k);
  cout << "gcd(" << a << ", " << b << ") = " << gcd << " = "
    << a << " * " << l << " + " << b << " * " << k << endl;
}
