// int c[n+1][m+1]   | n, m - dlugosci ciagów
// tab0[n] - pierwszy ciag
// tab1[m] - drugi ciag

/*  tablica c[n+1][m+1] po wykonaniu algorytmu
 *   a  b  b  a  b  c  c
 *  [0][0][0][0][0][0][0]
 * a[0][1][1][1][1][1][1]
 * c[0][1][1][1][1][2][2]
 * b[0][1][2][2][2][2][2]
 * a[0][1][2][3][3][3][3]
 * b[0][1][2][3][4][4][4]
*/ 

int count(int n)
{
    for(int i = 0; i <= n; ++i)
    {
        c[0][i] = 0;
        c[i][0] = 0;
    }
    for(int i = 1; i <= n; ++i)
    {
        for(int j = 1; j <= n; ++j)
        {
            if(tab0[i] == tab1[j])
            {
                c[i][j] = c[i-1][j-1] + 1;
            }
            else
            {
                c[i][j] = MAX(c[i-1][j], c[i][j-1]);
            }
        }
    }
    return c[n][n];
}
