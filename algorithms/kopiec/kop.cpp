#include <iostream>
using namespace std;

struct kop
{
        int size;
        int n;
        int *tab;
        kop(int s=0) : size(s), n(0)
        {
                tab = new int[s+1];
        }
        ~kop()
        {
                delete[] tab;
        }
        void repair(int v)
        {
                if (v > n) return;
                int k = tab[v];
                int tmp_i = v;
                int left = v * 2;
                int right = left + 1;
                if (left <= n && tab[left] > k) {
                        tmp_i = left;
                        k = tab[left];
                }
                if (right <= n && tab[right] > k) {
                        tmp_i = right;
                        k = tab[right];
                }
                if (tmp_i != v) {
                        tab[tmp_i] = tab[v];
                        tab[v] = k;
                        repair(tmp_i);
                }
        }
        void build()
        {
                int k = n / 2;
                for (int i = k; i >= 1; --i) {
                        repair(i);
                }
        }
        void insert(int a)
        {
                ++n;
                tab[n] = a;
                build();
        }
        void remove(int v)
        {
                if (v > n) {
                        return;
                }
                tab[v] = tab[n];
                --n;
                build();
        }
};

int main()
{
        ios_base::sync_with_stdio(0);
        kop kopiec(1000);
        int a;
        int c;
        while (cin >> c >> a) {
                if (c == 1) {
                        kopiec.insert(a);
                } else {
                        kopiec.remove(a);
                }
                for (int i = 0; i <= kopiec.n; ++i) {
                        cout << kopiec.tab[i] << ' ';
                }
                cout << endl;
        }
        return 0;
}
