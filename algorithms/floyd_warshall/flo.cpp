/* algorytm Floyda-Warshalla
 * znajdowanie najkrótszych
 * ścieżek pomiędzy wszystkimi
 * parami wierzchołków 
 * w grafie ważonym */
#include <iostream>
#include <vector>
#include <set>
#include <iomanip>
using namespace std;

typedef long long LL;
typedef vector<pair<int, int> > VP;
#define REP(x, n) for (int x = 0; x < n; ++x)
#define FOR(x, b, n) for (int x = b; x <= n; ++x)
#define SIZE(c) int((c).size())
#define PB push_back
const int SIZE = 100;
const int INF = 10e5;
VP G[SIZE];
int d[SIZE][SIZE]; // tablica odległości
int p[SIZE][SIZE]; // tablica poprzedników

void init()
{
        REP(i, SIZE) {
                REP(j, SIZE) {
                        if (i == j) d[i][j] = 0;
                        else d[i][j] = INF;
                }
        }
}

void floyd_warshall(int n) // n - ilosc wierzchokow w grafie
{
        FOR(i, 1, n) {
                FOR(j, 1, n) {
                        FOR(k, 1, n) {
                                if (d[j][k] > d[j][i] + d[i][k]) { /* jeżeli złożenie
                                                                      dwóch ścieżek jest korzystniejsze
                                                                      od aktualnej ścieżki to łączymy */
                                        d[j][k] = d[j][i] + d[i][k];
                                        p[j][k] = i;
                                }
                        }
                }
        }
}

void wypisz(int u, int v)
{
        if (p[u][v] != 0) {
                wypisz(u, p[u][v]);
                cout << p[u][v] << endl;
        }
}

int main()
{
        ios_base::sync_with_stdio(0);
        int n, m;
        cin >> n >> m;
        init();
        REP(i, m) {
                int a, b, c;
                cin >> a >> b >> c;
                G[a].PB(pair<int, int>(b, c));
                // G[b].PB(pair<int, int>(a, c)); // dla grafu nieskierowanego
                d[a][b] = c;
                // d[b][a] = c; // dla grafu nieskierowanego 
                p[a][b] = a;
        }
        floyd_warshall(n);
        FOR(i, 1, n) {
                FOR(j, 1, n) {
                        cout << d[i][j] << ' ';
                }
                cout << endl;
        }
        int u, v;
        cin >> u >> v;
        cout << "Droga z u do v: " << endl;
        wypisz(u, v);
        cout << v << endl;
        return 0;
}
