#include <iostream>
using namespace std;

class Kopiec
{
        private:
                int *t; // tablica do przechowywania elementow
                int l; // liczba elementow
        public:
                Kopiec(int nMax)
                {
                        t = new int[nMax+1];
                        l = 0;
                }
                ~Kopiec()
                {
                        delete[] t;
                }
                void Wstaw(int x);
                int Obsluz();
                void DoGory();
                void NaDol();
                void pisz();
};

void Kopiec::Wstaw(int x)
{
        t[++l] = x;
        DoGory();
}

void Kopiec::DoGory()
{
        int tmp = t[l];
        int n = l;
        while((n != 1) && (t[n/2] <= tmp))
        {
                t[n] = t[n/2];
                n = n / 2;
        }
        t[n] = tmp;
}

int Kopiec::Obsluz()
{
        int x = t[1];
        t[1] = t[l--];
        NaDol();
        return x;
}

void Kopiec::NaDol()
{
        int i = 1;
        while(1)
        {
                int p = 2 * i;  // lewy potomek wezla i to p, prawy p+1
                if(p > l)
                        break;
                if(p+1 <= l)    // prawy potomek niekoniecznie musi istniec
                        if(t[p] < t[p+1]) ++p; // przesuwamy sie do nastepnego
                if(t[i] >= t[p])
                        break;
                int tmp = t[p];
                t[p] = t[i];
                t[i] = tmp;
                i = p;
        }
}

int main()
{
        int a;
        Kopiec k(14);
}

