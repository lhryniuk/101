#include <iostream>
using namespace std;
typedef long long LL;

int a[1000];
int s[1000];
int t[1000];
int A, B, n, q, tmp;

int gcdw(int a, int b, int &l, int &k)
{
        if (!a) {
                l = 0;
                k = 1;
                return b;
        }
        int d = gcdw(b % a, a, k,  l);
        l -= (b / a) * k;
        return d;
}

int main()
{
        ios_base::sync_with_stdio(0);
        cin >> A >> B;
        a[n - 1] = gcdw(A, B, s[n - 1], t[n - 1]);
        cout << "NWD(" << A << "," << B << ") = " << a[n - 1] << endl;
        cout << s[n - 1] << " * " << A << " + ";
        cout << t[n - 1] << " * " << B << " = " << a[n - 1] << endl;
        return 0;
}
