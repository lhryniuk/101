#include <iostream>
#include <vector>
using namespace std;

#define PB push_back
#define SIZE(c) LL((c).size())

typedef long long LL;

const int SIZE = 10000;
vector<bool> IsPrime(SIZE+1);
vector<int> Primes;
LL phi_tab[SIZE];

// Sito Eratostenesa
void gen()
{
        LL i, j;
        for (i = 2; i <= SIZE; ++i) {
                IsPrime[i] = true;
        }
        for (i = 2; i * i <= SIZE; ++i) {
                if (IsPrime[i]) {
                        for (j = i * i; j <= SIZE; j += i) {
                                IsPrime[j] = false;
                        }
                }
        }
}

/* Wrzuca do wektora Primes kolejne
 * liczby pierwsze bazując na wektorze
 * IsPrime wygenerowanym sitem
 * Eratostenesa */
void gimme_primes()
{
        for (LL i = 2; i <= SIZE; ++i) {
                if (IsPrime[i]) {
                        Primes.PB(i);
                }
        }
}

LL phi(LL n)
{
        LL nn = n;
        if (n <= 1) return n;
        if (n == 2) return 1;
        if (nn < SIZE && phi_tab[nn]) return phi_tab[nn];
        LL wynik = 1, p;
        int x = Primes.size();
        for (int i = 0; i < x; ++i) {
                p = Primes[i];
                if (!(n % p)) {
                        LL q = 1, qq = p;
                        n /= p;
                        while(!(n % p)) {
                                q = qq;
                                qq *= p;
                                n /= p;
                        }
                        wynik *= qq - q;
                }
        }
        if (nn < SIZE) phi_tab[nn] = wynik;
        return wynik;
}

int main()
{
        ios_base::sync_with_stdio(0);
        gen();
        gimme_primes();
        int a;
        while (cin >> a) {
                cout << phi(a) << endl;
        }
}
