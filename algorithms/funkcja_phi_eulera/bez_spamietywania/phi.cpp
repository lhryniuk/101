#include <iostream>
using namespace std;

typedef long long LL;

LL phi(LL n)
{
        if (n <= 1) return n;
        if (n == 2) return 1;
        for (LL i = 2; i * i <= n; ++i) {
                if (n % i == 0) {
                        LL q = 1, qq = i;
                        n /= i;
                        while (n % i == 0) {
                                q = qq;
                                qq *= i;
                                n /= i;
                        }
                        return (qq - q) * phi(n);
                }
        }
        return n - 1;
}

int main()
{
        ios_base::sync_with_stdio(0);
        LL n;
        while (cin >> n) {
                cout << phi(n) << endl;
        }
}
