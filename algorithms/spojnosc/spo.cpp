/* sprawdzanie spojnosci metoda
 * przeszukiwania wglab
 * jezeli liczba odwiedzonych
 * wierzcholkow wychodzac z dowolnego
 * jest mniejsza od ilosci wszystkich
 * graf jest niespojny
 * */
#include <iostream>
#include <vector>
using namespace std;

#define PB push_back
#define REP(x, n) for (int x = 0; x < n; ++x)
#define SIZE(c) int((c).size())

const int SIZE = 100;
vector<int> G[SIZE];
bool odw[SIZE];
int nw = 0;

void dfs(int v)
{
    if (odw[v]) return;
    odw[v] = true;
    ++nw;
    int s = SIZE(G[v]);
    REP(i, s) {
        if (!odw[G[v][i]]) {
            dfs(G[v][i]);
        }
    }
}

int main()
{
    ios_base::sync_with_stdio(0);
    int n, m; /* n - ilosc wierzcholkow
                 m - ilosc krawedzi */
    cin >> n >> m;
    REP(i, m) {
        int a, b;
        cin >> a >> b;
        G[a].PB(b);
        // G[b].PB(a); // odkomentowac dla grafu nieskierowanego
    }
    dfs(1);
    if (nw == n) cout << "Graf jest spojny" << endl;
    else cout << "Graf jest niespojny" << endl;
}
