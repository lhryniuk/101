#include <iostream>
#include <vector>
#include <cmath>
using namespace std;

#define PB push_back
#define SIZE(c) int((c).size())
#define REP(x, n) for(int x = 0; x < n; ++x)

typedef vector<POINT> VP;

struct POINT
{
        double x;
        double y;
        POINT(double x, double y) : x(x), y(y) { }
};


int pole(VP& v)
{
        double area = 0;
        int s = SIZE(v);
        REP(i, s) {
                area += (v[i].x + v[(i+1)%s].x) * (v[i].y - v[(i+1)%s].y);
        }
        return abs(area) / 2;
}

int main()
{
        int k;
        double x, y;
        cin >> k;
        REP(i, k)
        {
                cin >> x >> y;
                v.PB(POINT(x, y));
        }
        cout << pole(v) << endl;
}
