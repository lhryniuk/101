/* transponowanie:
 * 1) dopisujemy do kazdego elementu nr kolumny i nr wiersza
 * 2) sortujemy, najpierw wg nr kolumny, nastepnie wg nr wiersza 
 * 3) zapisujemy w macierzy wyjsciowej */
#include <algorithm>
#include <iostream>
using namespace std;

struct num
{
  /* k - nr kolumny
   * w - nr wiersza */
  int k;
  int w;
  int n;
};

int m1[1000][1000];
int m2[1000][1000];
num tab[1000000];

bool cmp(num a, num b)
{
  if (a.k < b.k) return true;
  else if (a.k == b.k) return a.w < b.w;
  return false;
}

int main(void)
{
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      cin >> m1[i][j];
      int indeks = i * m + j;
      tab[indeks].n = m1[i][j];
      tab[indeks].w = i;
      tab[indeks].k = j;
    }
  }
  sort(tab, tab + n * m, cmp);
  for (int i = 0; i < m; ++i) {
    for (int j = 0; j < n; ++j) {
      m2[i][j] = tab[i * n + j].n;
      cout << m2[i][j] << ' ';
    }
    cout << '\n';
  }
  return 0;
}
