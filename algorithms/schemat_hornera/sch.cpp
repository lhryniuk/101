#include <iostream>
using namespace std;

double tab[100]; // współczynniki wielomianu

int wart(double x, int n) // obliczanie wartości wielomianu
                       // stopnia n argumentu x
{
    double s = tab[0];
    for (int i = 1; i < n; ++i) {
        s *= x;
        s += tab[i];
    }
    return s;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int n;
    cin >> n;
    for (int i = 0; i < n; ++i) {
        cin >> tab[i];
    }
    int x;
    cin >> x;
    cout << wart(x, n) << endl;
}
