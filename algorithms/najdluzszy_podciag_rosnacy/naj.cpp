#include <cstdio>
#include <iostream>
#include <algorithm>
#include <vector>
#include <queue>
using namespace std;

typedef long long LL;
typedef unsigned long long ULL;
typedef vector<int> VI;
typedef vector<string> VS;

#define FOR(x, b, e) for (int x = b; x <= (e); ++x)
#define FORD(x, b, e) for (int x = b; x >= (e); --x)
#define REP(x, n) for (int x = 0; x < n; ++x)
#define ALL(c) (c).begin(), (c).end()
#define SIZE(c) ((int)(c).size())
#define PB push_back
const int INF = 100000001;
const double EPS = 10e-9;

vector<int> V;

inline int LAS()
{
        int L = 0, n, a, ix;
        cin >> n;
        V.resize(n + 1, 1000000);
        V[0] = 0;
        REP (i, n) {
                cin >> a;
                ix = int(lower_bound(ALL(V), a) - V.begin());
                V[ix] = a;
                L = max(L, ix);
        }
        return L;
}

int main()
{
        ios_base::sync_with_stdio(0);
        cout << LAS() << endl;
        return 0;
}

