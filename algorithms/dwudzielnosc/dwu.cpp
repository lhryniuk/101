#include <iostream>
#include <vector>
using namespace std;

#define PB push_back
#define SIZE(c) int((c).size())
#define REP(x, n) for (int x = 0; x < n; ++x)

const int SIZE = 1000;
int colour[SIZE+1];
bool visit[SIZE+1];
vector<int> G[SIZE+1];
bool dwu;

void dfs(int v, int s, int n = 2) // n-dzielnosc
{
    if (s == 0) ++s;
    if (visit[v]) return;
    visit[v] = true;
    colour[v] = s;
    int siz = SIZE(G[v]);
    REP(i, siz) {
        if (colour[G[v][i]] == s) dwu = false;
        if (!visit[G[v][i]]) {
            dfs(G[v][i], (s+1) % (n+1), n);
        }
    }
}

int main()
{
    ios_base::sync_with_stdio(0);
    int n, m;
    int a, b;
    cin >> n >> m;
    REP(i, m) {
        cin >> a >> b;
        G[a].PB(b);
        G[b].PB(a);
    }
    dwu = true;
    dfs(1, 1);
    if (dwu) cout << "Graf jest dwudzielny" << endl;
    else cout << "Graf nie jest dwudzielny" << endl;
}
