#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>
#include <queue>
using namespace std;

typedef long long LL;
typedef unsigned long long ULL;
typedef vector<int> VI;
typedef vector<string> VS;

#define FOR(x, b, e) for (int x = b; x <= (e); ++x)
#define FORD(x, b, e) for (int x = b; x >= (e); --x)
#define REP(x, n) for (int x = 0; x < n; ++x)
#define ALL(c) (c).begin(), (c).end()
#define SIZE(c) ((int)(c).size())
#define PB push_back
const int INF = 100000001;
const double EPS = 10e-9;

struct POINT
{
  int x;
  int y;
  POINT(int x=0, int y=0) : x(x), y(y) { }
  void operator=(const POINT& a) {
    x = a.x;
    y = a.y;
  }
};

LL det(POINT a, POINT b, POINT c)
{
  return LL(b.x - a.x) * LL(c.y - a.y) - LL(c.x - a.x) * LL(b.y - a.y);
}

POINT* C;

bool cmp(POINT a, POINT b)
{
  LL w = det((*C), a, b);
  if (w == 0) return abs(C->x - a.x) + abs(C->y - a.y) < 
    abs(C->x - b.x) + abs(C->y - b.y);
  return w > 0;
}

vector<POINT> P;
vector<POINT> S;
vector<POINT> r;

void angle_sort(vector<POINT>& p, POINT a, POINT b)
{
  REP (i, SIZE(p)) {
    LL d = det(a, b, p[i]);
    (d > 0 || (d == 0 && (a.x == p[i].x ? a.y < p[i].y : a.x < p[i].x)))
      ? S.PB(P[i]) : r.PB(P[i]);
  }
  stable_sort(ALL(S), cmp);
  stable_sort(ALL(r), cmp);
  REP (i, SIZE(r)) {
    S.PB(r[i]);
  }
}

int main()
{
  ios_base::sync_with_stdio(0);
  int n;
  cin >> n;
  int x, y;
  REP (i, n) {
    cin >> x >> y;
    P.PB(POINT(x, y));
  }
  POINT a(0, 0), b(1, 0);
  C = new POINT();
  C->x = a.x;
  C->y = a.y;
  angle_sort(P, a, b);
  REP (i, SIZE(S)) {
    cout << S[i].x << ' ' << S[i].y << endl;
  }
  delete C;
  return 0;
}
