#include <cstdio>
#include <iostream>
#include <algorithm>
#include <cmath>
#include <string>
#include <vector>
#include <queue>
#include <list>
#include <set>
using namespace std;

typedef long long LL;
typedef unsigned long long ULL;
typedef vector<int> VI;
typedef vector<string> VS;

#define FOR(x, b, e) for (int x = b; x <= (e); ++x)
#define FORD(x, b, e) for (int x = b; x >= (e); --x)
#define REP(x, n) for (int x = 0; x < n; ++x)
#define ALL(c) (c).begin(), (c).end()
#define SIZE(c) ((int)(c).size())
#define PB push_back
#define debug (fmt, ...) fprintf(stderr, fmt, ## __VA__ARGS__)
const int INF = 100000001;
const double EPS = 10e-9;

struct POINT
{
        short int x;
        short int y;
        float cos;
        int d; 
        POINT(int x, int y, double cos, int d) : x(x), y(y), cos(cos), d(d) { }
};

bool cmp0(POINT a, POINT b)
{
        if (a.cos > b.cos) return true;
        if (a.cos == b.cos) return a.d < b.d;
        return false;
}

bool cmp1(POINT a, POINT b)
{
        if (a.cos < b.cos) return true;
        if (a.cos == b.cos) return a.d < b.d;
        return false;
}

vector<POINT> V0;
vector<POINT> V1;

int main()
{
        int n;
        scanf("%d\n", &n);
        REP (i, n) {
                short int x, y;
                scanf("%hd %hd\n", &x, &y);
                int d = x * x + y * y;
                double cos = ((x * x)/ (double(d)));
                if (x < 0) cos = -cos;
                if (y >= 0) {
                        V0.PB(POINT(x, y, cos, d));
                } else {
                        V1.PB(POINT(x, y, cos, d));
                }
        }
        stable_sort(ALL(V0), cmp0);
        REP (i, SIZE(V0)) {
                cout << V0[i].x << ' ' << V0[i].y << endl;
        }
        stable_sort(ALL(V1), cmp1);
        REP (i, SIZE(V1)) {
                cout << V1[i].x << ' ' << V1[i].y << endl;
        }
        return 0;
}
