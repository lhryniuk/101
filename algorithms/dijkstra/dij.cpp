#include <iostream>
#include <vector>
#include <set>
using namespace std;

typedef long long LL;
typedef vector<int> VI;
typedef vector< pair<int, int> > VP;
#define REP(x, n) for (int x = 0; x < n; ++x)
#define FOR(x, b, n) for (int x = b; x <= n; ++x)
#define SIZE(c) int((c).size())
#define PB push_back
#define ST first
#define ND second

const int SIZE = 1000;
const int INF = 10e7;
VP G[SIZE];
VI d;

struct cmp
{
  bool operator()(const int &a, const int &b)
  {
    if (d[a] < d[b]) return true;
    if (d[a] > d[b]) return false;
    return (a < b);
  }
};

set <int, cmp> K; // kopiec do dijkstry

void dijkstra(int v)
{
  /* inicjalizacja:
   * czyszczenie vectora z odleglosciami
   * zwiekszanie rozmiaru i inicjalizacja
   * nieskonczonoscia kazdego wierzcholka
   * wpisanie do wierzcholka startowego 0 */
  d.clear();
  d.resize(SIZE, INF);
  d[v] = 0;
  K.clear();
  FOR(i, 1, SIZE-1) K.insert(i); /* wrzucamy wszystkie wierzcholki do kopca */
  while (!K.empty()) {
    int u = *(K.begin()); /* najblizszy wierzcholek */
    K.erase(K.begin());
    int s = SIZE(G[u]);
    REP(i, s) {
      int v = G[u][i].ST; /* aktualnie przetwarzany krawedz */
      int c = G[u][i].ND; /* waga tejze krawedzi */
      if (d[u] + c < d[v]) { /* jezeli odleglosc jest korzystniejsza */
        K.erase(K.find(v)); /* usuwamy wierzcholek v (?) */
        d[v] = d[u] + c; /* aktualizujemy odleglosc */
        K.insert(v);
      }
    }
  }
}

void test()
{
  int n, m; // n - wierzcholki, m - krawedzie
  int a, b, w;
  cin >> n >> m;
  REP (i, m) {
    cin >> a >> b >> w;
    G[a].PB(pair<int,int>(b, w));
    G[b].PB(pair<int,int>(a, w));
  }
  int v;
  cout << "Podaj wierzcholek poczatkowy: " << endl;
  cin >> v;
  dijkstra(v);
  int c;
  cout << "Podaj cel: ";
  cin >> c;
  if (c < SIZE) {
    if (d[c] < INF)
      cout << "Droga z " << v << " do " << c << " ma dlugosc " << d[c] << endl;
    else {
      cout << "Z " << v << " do " << c << " nie prowadzi zadna droga." << endl;
    }
  }
}

int main()
{
  ios_base::sync_with_stdio(0);
  int t;
  cin >> t;
  REP(i, t) test();
}
