#include <iostream>
using namespace std;

void wypisz(bool *tab, int N)
{
    for (int i = 0; i < N; ++i) {
        cout << ((tab[i]) ? 1 : 0) << ' ';
    }
    cout << endl;
}

int intersection(bool *tab, int N)
{
    return 1;
}

int sum(int N)
{
    int answer = 0;
    int sign = 1;
    bool *A = new bool[N];
    A[0] = true;
    for (int i = 1; i <= N; ++i) {
        A[i] = false;
    }
    while (A[N] == 0) {
        answer += sign * intersection(A, N);
        int j = 0;
        while (A[j]) {
            A[j] = false;
            sign = -sign;
            ++j;
        }
        A[j] = true;
        sign = -sign;
    }
    return answer;
}

int main()
{
    sum(10);
}
