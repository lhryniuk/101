#include <iostream>
using namespace std;

typedef long long LL;

int SPM(int a, int b, int q) // a ^ b mod q
{
    LL p = 1;
    while (b > 0) {
        cout << "b = " << b << " p = " << p << " a = " << a << endl;
        if (b & 1) 
            p = (LL(a) * p) % q;
        a = (LL(a) * LL(a)) % q;
        b /= 2;
    }
    return p;
}

int main()
{
    int a, b, q;
    cin >> a >> b >> q;
    cout << SPM(a, b, q) << endl;
}

