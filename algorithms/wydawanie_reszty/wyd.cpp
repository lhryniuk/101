#include <iostream>
using namespace std;

const int k = 6001;
const int INF = 2 * 10e7;
/* pod T[i] ilosc monet potrzebna
 * do wydania i reszty */
int T[k];

int main()
{
        int n;
        int t;
        T[0] = 0;
        for (int i = 1; i < k; ++i) {
                T[i] = INF; /* 2000000 - nieskonczonosc */
        }
        cin >> t; // t - ilosc nominalow
        for (int j = 1; j <= t; ++j) {
                cin >> n; // n - nominal
                int g = k - n; /* przechodzimy od 0 do k - n */
                for (int i = 0; i <= g; ++i) {
                        if (T[i] < INF) { /* sprawdzamy, czy liczba monet
                                             potrzebna do wydania i reszty
                                             jest mniejsza od nieskonczonosci */
                                if (T[i] + 1 < T[i+n]) {
                                        T[i+n] = T[i] + 1; /* jezeli tak to dodajemy monete
                                                              o aktualnym nominale i dla
                                                              reszty i + n wpisujemy nowa
                                                              ilosc monet */
                                }
                        }
                }
        }
        for (int j = 0; j < k; ++j) {
                cout << T[j] << ' ';
        }
        cout << T[k-1] << endl; // ilosc monet potrzebna do wydania k-1 reszty
}
