// Funkcja prefiksowa Knutha
#include <iostream>
using namespace std;

#define FOR(x, b, n) for (int x = b; x <= n; ++x)
#define REP(x, n) for (int x = 0; x < n; ++x)
const int size = 1000002;
int p[size];

void knuth_pref_fun(string P)
{
    int t = 0;
    p[0] = p[1] = 0;
    FOR(i, 2, int(P.size())) {
        while (t > 0 && P[t] != P[i-1]) t = p[t];
        if (P[t] == P[i-1]) ++t;
        p[i] = t;
    }
}

int main()
{
    int n;
    cin >> n;
    REP(x, n) {
        string a;
        cin >> a;
        knuth_pref_fun(a);
        FOR(i, 1, int(a.size())) cout << p[i] << ' ';
        cout << endl;
    }
    return 0;
}
