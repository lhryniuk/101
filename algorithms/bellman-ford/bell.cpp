#include <iostream>
#include <algorithm>
#include <cstdio>
#include <vector>
#include <cmath>
#include <string>
using namespace std;

typedef long long LL;
typedef vector<int> VI;
#define PB push_back
#define SIZE(c) int((c).size())
#define ALL(c) (c).begin(), (c).end()
#define REP(x, n) for (int x = 0; x < n; ++x)
#define FOR(x, b, n) for (int x = b; x <= n; ++x)
#define FORD(x, b) for (int x = b; x >= 0; --x)

const int INF = 10000000;
const int SIZE = 101;
int w[SIZE][SIZE];
int d[SIZE];

bool bell(int n)
{
    // odleglosc do wierzcholka poczatkowego
    // (domyslnie 1)
    d[1] = 0;
    FOR(i, 2, n) {
        d[i] = INF;
    }
    // n-1-krotna relaksacja krawedzi
    REP(k, n - 1) {
        FOR(i, 1, n) {
            FOR(j, 1, n) {  
                if (d[j] > d[i] + w[i][j]) {
                    d[j] = d[i] + w[i][j];
                }
            }
        }
    }
    // n-ta relaksacja dla sprawdzenia istnienia
    // o ujemnej sumie wag krawedzi
    FOR(i, 1, n) {
        FOR(j, 1, n) {  
            if (d[j] > d[i] + w[i][j]) {
                return true;
            }
        }
    }
    return false;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int n, m;
    cin >> n >> m;
    REP(i, SIZE) {
        REP(j, SIZE) {
            w[i][j] = INF;
        }
    }
    REP(i, m) {
        int a, b, c;
        cin >> a >> b >> c;
        w[a][b] = c;
    }
    bool cykl = bell(n);
    if (cykl) cout << "cykl ujemny" << endl;
    else {
        FOR(i, 2, n) {
            if (d[i] != INF) cout << d[i] << endl;
            else cout << "nieosiagalny" << endl;
        }
    }
    return 0;
}
