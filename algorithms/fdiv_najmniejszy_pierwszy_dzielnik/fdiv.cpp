#include <iostream>
#include <vector>
using namespace std;

typedef long long LL;
const LL SIZE = 100000; // liczby
/* w tablicy fDiv zapisujemy pod fDiv[i]
 * najmniejszy dzielnik pierwszy liczby i */
vector<int> fDiv(SIZE+1);

void make_table()
{
        LL i;
        LL j;
        for (i = 2; i <= SIZE; ++i) {
                fDiv[i] = i;
        }
        for (i = 2; i <= SIZE; ++i) {
                if (fDiv[i] == i) {
                        for (j = i * i; j <= SIZE; j += i) {
                                if (fDiv[j] == j) {
                                        fDiv[j] = i;
                                }
                        }

                }
        }
}

int main()
{
        ios_base::sync_with_stdio(0);
        make_table();
        cout << fDiv[135] << endl;
}
