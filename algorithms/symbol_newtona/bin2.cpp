#include <iostream>
using namespace std;

typedef long long LL;

#define REP(x, n) for(int x = 0; x < (n); ++x)
#define FOR(x, b, e) for(int x = b; x <= (e); ++x)

LL Binom(int n, int k)
{
    #define Mark(x, y) for(int w = x, t = 2; w > 1; ++t)\
    while(!(w % t)) { w /= t; p[t] += y; }
    if(n < k || n < 0) return 0;
    int p[n + 1];
    REP(x, n + 1) p[x] = 0;
    FOR(x, n - k + 1, n) Mark(x, 1);
    FOR(x, 1, k) Mark(x, -1);
    LL r = 1;
    FOR(x, 1, n) while(p[x]--) r *= x;
    return r;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int n, k;
    cin >> n >> k;
    cout << Binom(n, k) << endl;
}
