#include <iostream>
using namespace std;
typedef long long LL;

#define FOR(x, b, e) for(int x = b; x <= (e); ++x)
#define REP(x, n) for(int x = 0; x < (n); ++x)

LL binom(int n, int k)
{
    #define Mark(x, y) for(int w = x, t = 2; w > 1; ++t) \
    while(!(w % t)) { w /= t; p[t] += y; }
    if(n < k || n < 0) return 0;
    int p[n + 1];
    REP(x, n + 1) p[x] = 0;
    // Wyznacz wartosc liczby n!/(n-k) != (n - k + 1) * ... * n w postaci
    // rozkładu na liczby pierwsze
    FOR(x, n - k + 1, n) Mark(x, 1);
    // Podziel liczbe, ktorej rozklad znajduje sie w tablicy p przez k!
    FOR(x, 1, k) Mark(x, -1);
    // Oblicz wartosc wspolczynnika dwumianowego na podstawie
    // jej rozkladu na liczby pierwsze i zwroc wynik
    LL r = 1;
    FOR(x, 1, n) while(p[x]--) r *= x;
    return r;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int n, k;
    while (cin >> n >> k) {
        cout << binom(n, k) << endl;
    }
    return 0;
}
