#include <iostream>
using namespace std;

#define REP(x, n) for (int x = 0; x < n; ++x)

const int S = 2;
typedef unsigned long long LL;

struct matrix
{
    LL a, b, c, d;
    matrix(LL a = 0, LL b = 1, LL c = 1, LL d = 0) : a(a), b(b), c(c), d(d) { }
    matrix operator*(matrix T)
    {
        matrix W(0, 0, 0, 0);
        W.a = (a * T.a) + (b * T.c);
        W.b = (a * T.b) + (b * T.d);
        W.c = (c * T.a) + (d * T.c);
        W.d = (c * T.b) + (d * T.d);
        return W;
    }
};

matrix power(matrix A, int n)
{
    matrix W(0, 1, 1, 1);
    while (n) {
        if (n % 2) W = W * A;
        A = A * A;
        n /= 2;
    }
    return W;
}

void test()
{
    matrix M(0, 1, 1, 1);
    int n;
    cin >> n;
    M = power(M, n);
    cout << M.a << endl;
}

int main()
{
    ios_base::sync_with_stdio(0);
    int t;
    cin >> t;
    while (t--) test();
}
