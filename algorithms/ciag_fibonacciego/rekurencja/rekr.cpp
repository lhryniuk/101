#include <iostream>
using namespace std;

typedef unsigned long long LL;

LL fib(LL n)
{
        if (n < 3) {
                return 1;
        }
        return fib(n - 1) + fib(n - 2);
}

int main()
{
        LL a;
        cin >> a;
        cout << fib(a) << endl;
        return 0;
}
