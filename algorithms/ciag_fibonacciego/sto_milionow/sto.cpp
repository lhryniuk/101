#include <iostream>
#include <gmp.h>
using namespace std;

int main()
{
        
        mpz_t a, c;
        mpz_t b;
        mpz_init(a);
        mpz_init(b);
        mpz_init(c);
        mpz_set_ui(a, 0);
        mpz_set_ui(b, 1);
        for (int i = 0; i < 10000000; ++i) {
                mpz_add(c, a, b);
                mpz_set(a, b);
                mpz_set(b, c);
        }
        cout << a << endl;
        return 0;
}
