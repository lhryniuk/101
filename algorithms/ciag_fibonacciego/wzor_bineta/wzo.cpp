#include <iostream>
#include <cmath>
using namespace std;

/* wzor Bineta:
 * Fn = (1 / sqrt(5)) * ((1 + sqrt(5) / 2) ^ n - ((1 - sqrt(5)) / 2)^n)
 * w przybliżeniu:
 * Fn ~ (1 / sqrt(5)) * ((1 + sqrt(5)) / 2)^n*/
int main()
{
        int n;
        double rt = sqrt(5.0);
        int t;
        cin >> t;
        while (t--) {
                cin >> n;
                cout << (unsigned long long)((1 / rt) * (pow(((1 + rt) / 2), n) - pow(((1 - rt) / 2), n))) << endl;
        }
}
