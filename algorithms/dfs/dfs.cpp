#include <cstdio>
#include <vector>
using namespace std;

const int ROZ = 100000;

#define SIZE(c) int((c).size())
#define PB push_back
#define FOR(x, n) for(int x = 0; x < n; ++x)

vector<int> G[ROZ];
bool odwiedzony[ROZ];

void sasiedzi(int);

void dfs(int v)
{
    if(odwiedzony[v]) return;
    odwiedzony[v] = true;
    sasiedzi(v);
}

void sasiedzi(int v)
{
    int x = SIZE(G[v]);
    FOR(i, x)
        dfs(G[v][i]);
}

int main()
{
    int n, m;
    int a, b;
    scanf("%d %d", &n, &m);
    FOR(i, m)
    {
        scanf("%d %d", &a, &b);
        G[a].PB(b);
        G[b].PB(a);
    }
    dfs(1);
}
