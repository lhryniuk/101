#include <iostream>
#include <vector>
using namespace std;

vector<vector<int> > G;
vector<int> V;

void dfs(int v)
{
        if (V[v]) return;
        V[v] = true;
        cout << "Odwiedzam wierzchołek " << v << endl;
        for (int i = 0; i < int(G[v].size()); ++i) {
                if (!V[G[v][i]]) {
                        dfs(G[v][i]);
                }
        }
}

int main()
{
        ios_base::sync_with_stdio(0);
        int n, m;
        cin >> n >> m;
        G.resize(n+1);
        V.resize(n+1);
        for (int i = 0; i < m; ++i) {
                int a, b;
                cin >> a >> b;
                G[a].push_back(b);
                G[b].push_back(a);
        }
        dfs(1);
        return 0;
}

