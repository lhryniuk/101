/* Algorytm Prima
- * Wyznaczanie minimalnego drzewa rozpinającego 
 * */
#include <iostream>
#include <vector>
#include <set>
using namespace std;

#define REP(x, n) for (int x = 0; x < n; ++x)
#define FOR(x, b, n) for (int x = b; x <= n; ++x)
#define PB push_back
#define SIZE(c) int((c).size())
#define ST first
#define ND second
typedef vector<int> VI;
typedef vector<bool> VB;
typedef vector<vector<pair<int, int> > > VP;
const int INF = 1000000;
const int SIZE = 100;
VP G; // G - przechowujemy graf
VI W, P; // W - wagi wierzcholkow, P - poprzednicy (krawedzie)
VB V; // V - lista odwiedzonych

/* struktura ustalajaca porzadek w kopcu S
 * */
struct cmp
{
    bool operator()(const int &a, const int &b) {
        if (W[a] < W[b]) return true;
        if (W[b] < W[a]) return false;
        return (a < b);
    }
};

set<int, cmp> S;

void prim(int s, int n)
{
    // przygotowanie zmiennych i struktur danych
    int v, u, c;
    W.clear();
    W.resize(n+1, INF);
    V.clear();
    V.resize(n+1, false);
    P.resize(n+1);
    W[s] = 0;
    P[s] = s;
    S.clear();
    FOR(i, 1, n) S.insert(i);
    while (!S.empty()) {
        u = *(S.begin()); // bierzemy najblizszy wierzcholek
        S.erase(S.begin());
        V[u] = true; // wierzcholek odwiedzony, dodajemy do MST
        REP(i, SIZE(G[u])) { // przechodzimy po sasiadach
            v = G[u][i].ST;
            if (!V[v]) { // jezeli nie odwiedzony
                c = G[u][i].ND; // czytamy wage
                if (c < W[v]) { // jezeli jest korzystniej uaktualniamy krawedz
                    S.erase(S.find(v));
                    W[v] = c;
                    S.insert(v);
                    P[v] = u;
                }
            }
        }
    }
}

int main()
{
    ios_base::sync_with_stdio(0);
    int n, m;
    int a, b, c;
    cin >> n >> m;
    G.resize(n+1);
    REP(i, m) {
        cin >> a >> b >> c;
        G[a].PB(make_pair(b, c));
        G[b].PB(make_pair(a, c));
    }
    prim(1, n);
    int suma = 0;
    FOR(i, 1, n) suma += W[i];
    cout << "Koszt MST: " << suma << endl;
    cout << "Drzewo zlozone jest z krawedzi: " << endl;
    for (int i = 2; i <= n; ++i) {
        cout << i << ' ' << P[i] << endl;
    }
}
