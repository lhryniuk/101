/* przesuniecie cyklicznie ciagu liczb dlugosci n
 * o k w lewo metoda zonglowania */
#include <iostream>
using namespace std;

const int SIZE = 1000000;
int num[SIZE]; // tablica do przechowywania liczb

int nwd(int a, int b)
{
  while (b)
    swap(a %=b, b);
  return a;
}

/* przesuwamy cyklicznie o k miejsc w lewo, o tak:
 * 1) zaczynamy od num[0], zapisujemy liczbe z zmiennej tymczasowej t
 * 2) przepisujemy:
 *    num[0] = num[i]
 *    num[i] = num[2*i]
 *    num[2*i] = num[3*i]
 *    ...
 *    az dojdziemy do num[0] (wszystkie indeksy liczymy mod n)
 * 3) wpisujemy na ostatnim miejscu, z ktorego przepisywalismy, t
 * 4) jesli nie przepisalismy wszystkich liczb, bierzemy num[1], itd.
 *    (nalezy zauwazyc, ze przepisujac, w jednym przelocie przejdziemy
 *    co nwd(k, n)-ta liczbe, zatem wybieramy na poczatkowe indeksy wszystkie
 *    od 0 do nwd(k, n) - 1
 *  
 * */
void przesun(int k, int n)
{
  for (int j = 0; j < nwd(k, n); ++j) {
    int t = num[j];
    int i = j;
    int l = k + j;
    while (l != j) {
      num[i] = num[l];
      /* przechodzimy do nastepnej pary */
      i += k;
      l += k;
      /* indeksy modulo n */
      i %= n;
      l %= n;
    }
    num[i] = t;
  }
}

/* dla sprawdzenia */
void wypisz(int n)
{
  for (int i = 0; i < n; ++i) {
    cout << num[i] << ' ';
  }
}

int main(void)
{
  int n = 0;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> num[i];
  }
  wypisz(n);
  cout << '\n';
  int k = 0;
  cin >> k;
  przesun(k, n);
  wypisz(n);
  return 0;
}
