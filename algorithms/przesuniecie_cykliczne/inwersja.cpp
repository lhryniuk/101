/* przesuniecie cyklicznie ciagu liczb dlugosci n
 * o k w lewo przez inwersje 
 * r - odwrocenie kolejnosci elementow
 * r(r(a)r(b)) = ba */
#include <iostream>
using namespace std;

const int SIZE = 1000000;
int num[SIZE];

void r(int a, int b)
{
  /* przechodzimy tylko do polowy i zamieniamy miejscami
   * elementy w rownej odleglosci od niej */
  int limit = a + (b - a) / 2;
  for (int i = a; i <= limit; ++i) {
    int l = i - a;
    swap(num[i], num[b - l]);
  }
}

void wypisz(int n);
void przesun(int k, int n) // k i n z opisu na gorze
{
  r(0, k-1);
  r(k, n-1);
  r(0, n-1);
}

/* dla sprawdzenia */
void wypisz(int n)
{
  for (int i = 0; i < n; ++i) {
    cout << num[i] << ' ';
  }
  cout << '\n';
}

int main(void)
{
  int n = 0;
  cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> num[i];
  }
  wypisz(n);
  int k = 0;
  cin >> k;
  przesun(k, n);
  wypisz(n);
  return 0;
}

