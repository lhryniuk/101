#include <iostream>
#include <vector>

int main(void)
{
  std::ios_base::sync_with_stdio(0);
  int t = 0;
  std::cin >> t;
  while (t--) {
    int n = 0;
    std::cin >> n;
    std::vector<int> V(n, 0);
    int ile = 0;
    int l = 0;
    for (int i = 0; i < n; ++i) {
      int a;
      std::cin >> a;
      V[i] = a;
      if (ile == 0) {
        l = a;
        ile = 1;
      }
      if (V[i] == l) {
        ++ile;
      } else {
        --ile;
      }
    }
    int zl = 0;
    for (int i = 0; i < n; ++i) {
      if (V[i] == l) {
        ++zl;
      }
    }
    if (zl > n / 2) {
      std::cout << l << std::endl;
    } else {
      std::cout << -1 << std::endl;
    }
  }
  return 0;
}
