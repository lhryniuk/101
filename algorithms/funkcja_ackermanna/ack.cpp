#include <iostream>
using namespace std;

typedef long long LL;

inline LL acker(LL m, LL n)
{
    if (m == 0) return n + 1;
    if (m > 0 && n == 0) return acker(m - 1, 1);
    return acker(m - 1, acker(m, n - 1));
}

int main()
{
    LL m, n;
    cin >> m >> n;
    cout << acker(m, n) << endl;
}
