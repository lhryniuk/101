#pragma once

#include <memory>
#include <unordered_set>
#include <vector>
#include "types.hpp"

namespace Impl
{
struct Tree
{
    struct Node
    {
        Node(unsigned char c)
        {
            c_ = c; 
        }
        unsigned char c_;
        bool end_of_word_{false};
        std::unique_ptr<Node> l_{nullptr};
        std::unique_ptr<Node> e_{nullptr};
        std::unique_ptr<Node> r_{nullptr};
        std::unordered_set<std::string> files_;
    };

    void AddFileToWord(std::string&& word, const std::string& filename);
    void Traverse(void);
    FilesList GetFilesForWord(const std::string& word);

    std::unique_ptr<Node> root_{nullptr};

private:
    void AddFileToWord(std::string&& word, std::string::const_iterator pos, std::unique_ptr<Node>& node, const std::string& filename);
    void Traverse(std::string& word, std::string::iterator pos, const std::unique_ptr<Node>& node);
    std::unordered_set<std::string> GetFilesForWord(const std::string& word, std::string::const_iterator pos, const std::unique_ptr<Node>& node);
};
}
