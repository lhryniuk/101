#include "Tree.hpp"

#include <iostream>

namespace Impl
{

void Tree::AddFileToWord(std::string&& word, const std::string& filename)
{
    AddFileToWord(std::move(word), std::begin(word), root_, filename);
}

void Tree::Traverse(void)
{
    std::string word;
    word.resize(100);
    Traverse(word, std::begin(word), root_);
}

FilesList Tree::GetFilesForWord(const std::string& word)
{
    return GetFilesForWord(word, std::begin(word), root_);
}

void Tree::AddFileToWord(std::string&& word, std::string::const_iterator pos, std::unique_ptr<Node>& node, const std::string& filename)
{
    if (node == nullptr) {
        node = std::make_unique<Node>(static_cast<unsigned char>(*pos));
    }
    if (static_cast<unsigned char>(*pos) < node->c_) {
        AddFileToWord(std::move(word), pos, node->l_, filename); 
    } else if (static_cast<unsigned char>(*pos) > node->c_) {
        AddFileToWord(std::move(word), pos, node->r_, filename); 
    } else {
        if (pos != std::end(word)) {
            AddFileToWord(std::move(word), ++pos, node->e_, filename);
        } else {
            node->end_of_word_ = true; 
            node->files_.insert(filename);
        } 
    }
}

void Tree::Traverse(std::string& word, std::string::iterator pos, const std::unique_ptr<Node>& node)
{
    if (node) {
        Traverse(word, pos, node->l_);
        *pos = node->c_;
        if (node->end_of_word_) {
            std::cout << std::string(std::begin(word), pos) << '\n'; 
        }
        Traverse(word, pos+1, node->e_);
        Traverse(word, pos, node->r_);
    }
}

std::unordered_set<std::string> Tree::GetFilesForWord(const std::string& word, std::string::const_iterator pos, const std::unique_ptr<Node>& node)
{
    if (pos == std::end(word) && node->end_of_word_) {
        return node->files_; 
    }
    if (!node)
        return std::unordered_set<std::string>{};

    if (*pos < node->c_) {
        return GetFilesForWord(word, pos, node->l_); 
    } else if (*pos > node->c_) {
        return GetFilesForWord(word, pos, node->r_); 
    } else {
        return GetFilesForWord(word, pos + 1, node->e_);
    }

}

}
