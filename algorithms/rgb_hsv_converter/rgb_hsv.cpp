/*
 * Author: lukequaint
 */

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include <ctime>
#include <cmath>
#include <cstdlib>
#include <string>
#include <iostream>
using namespace std;

typedef unsigned long long LL;

const int WIDTH = 1000;
const int HEIGHT = 900;

SDL_Surface* load_image(string filename)
{
  SDL_Surface* loaded_image = NULL;
  SDL_Surface* optimized_image = NULL;
  loaded_image = IMG_Load(filename.c_str()); 
  if (loaded_image != NULL) {
    optimized_image = SDL_DisplayFormat(loaded_image);
    SDL_FreeSurface(loaded_image);
  }
  return optimized_image;
}

void apply_surface(int x, int y, SDL_Surface* source, SDL_Surface* destination)
{
  SDL_Rect offset;
  offset.x = x;
  offset.y = y;
  SDL_BlitSurface(source, NULL, destination, &offset);
}

/*
 * colours
 */
struct RGB
{
  int red;
  int green;
  int blue;
  double R;
  double G;
  double B;
  RGB (double mR = 0, double mG = 0, double mB = 0) : red(mR), green(mG), blue(mB), R(mR), G(mG), B(mB) { }
  void Set(double mR, double mG, double mB) {
    R = mR;
    G = mG;
    B = mB;
    red = round(R);
    green = round(G);
    blue = round(B);
  }
};

struct HSV
{
  double H;
  double S;
  double V;
  HSV(double mH = -1, double mS = 0, double mV = 0) : H(mH), S(mS), V(mV) { }
};

struct Color
{
  RGB rgb;
  HSV hsv;
  void RGB2HSV()
  {
    double mini, maxi, delta;
    mini = min(min(rgb.R, rgb.G), rgb.B);
    maxi = max(max(rgb.R, rgb.G), rgb.B);
    delta = ((maxi - mini) / 255);
    hsv.V = maxi;
    if (maxi != 0) {
      hsv.S = (delta / maxi);
    } else {
      hsv.S = 0;
      hsv.H = -1;
      return;
    }
    if (rgb.R == maxi) {
      hsv.H = (rgb.G - rgb.B) / delta;
    } else if (rgb.G == maxi) {
      hsv.H = 2 + (rgb.B - rgb.R) / delta;
    } else {
      hsv.H = 4 + (rgb.R - rgb.G) / delta;
    }
    hsv.H *= 60;
    if (hsv.H < 0) {
      hsv.H += 360;
    }
  }
  void HSV2RGB()
  {
    double red;
    double green;
    double blue;
    red = green = blue = 0.0;
    if (hsv.V == 0) {
      rgb.Set(red, green, blue);
      return;
    }
    double hue = (hsv.H / 60.0);
    int hp = int(hue);
    double f = hue - hp;
    double p = hsv.V * (1 - hsv.S);
    double q = hsv.V * (1 - hsv.S * f);
    double t = hsv.V * (1 - hsv.S * (1 - f));
    if (hp == 0) {
      red = hsv.V;
      green = t;
      blue = p;
    } else if (hp == 1) {
      red = q;
      green = hsv.V;
      blue = p;
    } else if (hp == 2) {
      red = p;
      green = hsv.V;
      blue = t;
    } else if (hp == 3) {
      red = p;
      green = q;
      blue = hsv.V;
    } else if (hp == 4) {
      red = t;
      green = p;
      blue = hsv.V;
    } else if (hp == 5) {
      red = hsv.V;
      green = p;
      blue = q;
    }
    rgb.Set(red * 255.0, green * 255.0, blue * 255.0);
  }
  void ChangeSaturation(double d)
  {
    hsv.S *= 100.0;
    hsv.S += d;
    hsv.S /= 100.0;
    while (hsv.S > 1.0) hsv.S = 1.0;
    while (hsv.S < 0.0) hsv.S = 0.0;
    HSV2RGB();
  }
  void ChangeHue(double d)
  {
    if (hsv.H == -1) hsv.H = 0;
    hsv.H += d;
    while (hsv.H >= 360) hsv.H = 359;
    while (hsv.H < 0) hsv.H = 0;
    HSV2RGB();
  }
  void ChangeValue(double d)
  {
    hsv.V *= 100.0;
    hsv.V += d;
    hsv.V /= 100.0;
    while (hsv.V > 1.0) hsv.V = 1.0;
    while (hsv.V < 0.0) hsv.V = 0.0;
    HSV2RGB();
  }
};

int main(int argc, char* argv[])
{
  SDL_Event event;
  SDL_Surface* screen = NULL;
  SDL_Init(SDL_INIT_EVERYTHING);
  screen = SDL_SetVideoMode(WIDTH, HEIGHT, 32, SDL_SWSURFACE);
  Color col;
  SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, int(col.rgb.R), int(col.rgb.G), int(col.rgb.B)));
  bool quit = false;
  bool change = false;
  col.ChangeHue(280);
  col.ChangeValue(0);
  col.ChangeSaturation(90);
  while (!quit) {
    if (SDL_PollEvent(&event)) {
      if (event.type == SDL_KEYDOWN && event.key.keysym.sym == SDLK_ESCAPE) {
        quit = true;
      }
    }
    Uint8 *keystates = SDL_GetKeyState(NULL);
    if (keystates[SDLK_w]) {
      col.ChangeValue(1);
    }
    if (keystates[SDLK_s]) {
      col.ChangeValue(-1);
    }
    if (keystates[SDLK_q]) {
      col.ChangeHue(1);
    }
    if (keystates[SDLK_a]) {
      col.ChangeHue(-1);
    }
    if (keystates[SDLK_e]) {
      col.ChangeSaturation(1);
    }
    if (keystates[SDLK_d]) {
      col.ChangeSaturation(-1);
    }
    SDL_FillRect(screen, &screen->clip_rect, SDL_MapRGB(screen->format, int(col.rgb.R), int(col.rgb.G), int(col.rgb.B)));
    SDL_Flip(screen);
  }
  return 0;
}
