#include <iostream>
using namespace std;

typedef long long LL;

int gcdw(int a, int b, LL &l, LL &k)
{
        if (!a) {
                l = 0;
                k = 1;
                return b;
        }
        int d = gcdw(b % a, a, k, l);
        l -= (b / a) * k;
        return d;
}

/* rozwiazuje kongurencje
 * a * x = 1 (mod m) 
 * */
int rev(int a, int m) 
{
        LL x, y;
        if (gcdw(a, m, x, y) != 1) return -1;
        x %= m;
        if (x < 0) x += m;
        return x;
}

int main()
{
        ios_base::sync_with_stdio(0);
        int a, m;
        cout << "a * x = 1 (mod m)" << endl << "Podaj a i m: ";
        while (cin >> a >> m) {
                int x;
                if ((x = rev(a, m)) != -1)
                        cout << "x = " << x << endl;
                else
                        cout << "Brak rozwiazan!" << endl;
        }
        return 0;
}
