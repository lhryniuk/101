#include <array>
#include <iostream>
#include <type_traits>
#include <vector>
using namespace std;

template<unsigned long long N, typename T=unsigned long long, typename JoinSmallerToBigger=bool>
struct UnionFindT
{
    static_assert(std::is_trivial<T>::value, "lol");
    UnionFindT()
    {
        for (auto i = 0; i < size_; ++i) {
            set_of_[i] = i;
            size_of_[i] = 1;
        }
    }
    T Find(unsigned long long x)
    {
        if (set_of_[x] == x) {
            return x;
        }
        set_of_[x] = Find(set_of_[x]);
        return set_of_[x];
    }
    /* smaller set will be a part a bigger set */
    template <typename S=JoinSmallerToBigger>
    typename std::enable_if<std::is_same<S, bool>::value, bool>::type Union(unsigned long long x, unsigned long long y)
    {
        int a = x, b = y;
        if (Find(a) == Find(b)) {
            return false;
        }
        if (size_of_[Find(a)] > size_of_[Find(b)]) {
            a = y; b = x;
        }
        set_of_[Find(a)] = Find(b);
        size_of_[b] += size_of_[a];
        return true;
    }
    /* y set will be a part x set */
    template <typename S=JoinSmallerToBigger>
    typename std::enable_if<not std::is_same<S, bool>::value, bool>::type Union(unsigned long long x, unsigned long long y)
    {
        if (Find(x) == Find(y)) return false;
        set_of_[Find(y)] = Find(x);
        size_of_[x] += size_of_[y];
        return true;
    }
    const unsigned long long size_ = N;
    std::array<T, N> set_of_;
    std::array<T, N> size_of_;
};

template<unsigned long long N>
using UnionFind = UnionFindT<N>;

int main()
{
    UnionFind<10> uf;
    int a, b;
    a = uf.Find(5);
    b = uf.Find(6);
    if (a != b) {
        std::cout << "a i b are in different sets" << endl;
    } else {
        std::cout << "a i b are in the same set" << endl;
    }
    std::cout << a << ' ' << b << endl;
    uf.Union(a, b);
    a = uf.Find(5);
    b = uf.Find(6);
    if (a != b) {
        std::cout << "a i b are in different sets" << endl;
    } else {
        std::cout << "a i b are in the same set" << endl;
    }
    std::cout << a << ' ' << b << endl;
    uf.Union(0, 8);
    uf.Union(3, 4);
    uf.Union(4, 5);
    if (uf.Find(3) == uf.Find(5)) {
        std::cout << "3 i 5 are in the same set" << endl;
    }
    std::cout << uf.size_of_[5] << endl;
}
