/* Schemat Hornera - wyliczanie wartosci
 * i dzielenie wielomianu przez dwumian
 * x - a */

#include <iostream>
using namespace std;

const int ROZMIAR = 100;
int tmp[ROZMIAR];

struct WIELOMIAN 
{
        int n; // stopien wielomianu
        int *wspol; // wspolczynniki przy kolejnych potegach
        WIELOMIAN()
        {
                wspol = new int[ROZMIAR];
        }
        ~WIELOMIAN()
        {
                delete[] wspol;
        }
        friend istream& operator>>(istream &str, WIELOMIAN &w) 
        {
                cout << "Podaj stopien wielomianu: ";
                str >> w.n;
                cout << "Podaj wspolczynniki przy kolejnych wyrazach" << endl;
                cout << "zaczynajac od najwiekszej potegi (a^n): " << endl;
                for (int i = 0; i <= w.n; ++i) {
                        str >> w.wspol[i];
                }
                return str;
        }
        friend ostream& operator<<(ostream &str, WIELOMIAN &w) 
        {
                str << "Stopien: " << w.n << endl;
                for (int i = 0; i <= w.n; ++i) {
                        if (w.wspol[i] != 0) {
                                if (i != 0)
                                        str << showpos;
                                else
                                        str << noshowpos;
                                str << w.wspol[i];
                                if (w.n - i) {
                                        str << "*";
                                        str << "(x^" << noshowpos << w.n - i << ")";
                                }
                        }
                }
                str << endl;
                return str;
        }
};

int wartosc(WIELOMIAN &w, int x)
{
        int wyn = w.wspol[0];
        for (int i = 1; i <= w.n; ++i) {
                wyn *= x;
                wyn += w.wspol[i];
        }
        return wyn;
}

void dzielenie(WIELOMIAN &w, int a)
{
        tmp[0] = w.wspol[0];
        int x;
        for (int i = 0; i < w.n; ++i) {
                x = tmp[i] * a;
                tmp[i+1] = w.wspol[i+1] + x; 
        }
        if (tmp[w.n])
                cout << "Reszta z dzielenia = " << tmp[w.n] << endl;
        for (int i = 0; i < w.n; ++i) {
                w.wspol[i] = tmp[i];
        }
        --w.n;
}

int main(int argc, char* argv[])
{
        WIELOMIAN W;
        cin >> W;
        cout << W;
        // cout << wartosc(W, 2) << endl;
        int dziel;
        cout << "Dzielimy wielomian przez dwumian x - a. Podaj a: ";
        cin >> dziel;
        dzielenie(W, dziel);
        cout << W;
}
