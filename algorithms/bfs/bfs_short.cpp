#include <iostream>
#include <vector>
#include <queue>
using namespace std;

typedef long long LL;
vector<vector<int> > G;
vector<bool> V;
queue<int> Q;

void bfs(int v)
{
  if (V[v]) return;
  V[v] = true;
  Q.push(v);
  while (!Q.empty()) {
    int u = Q.front();
    Q.pop();
    for (int i = 0; i < int(G[u].size()); ++i) {
      if (!V[G[u][i]]) {
        Q.push(G[u][i]);
        V[G[u][i]] = true;
      }
    }
  }
}

int main()
{
  ios_base::sync_with_stdio(0);
  int n, m;
  cin >> n >> m;
  G.resize(n+1);
  V.resize(n+1, false);
  for (int i = 0; i < m; ++i) {
    int a, b;
    cin >> a >> b;
    G[a].push_back(b);
    G[b].push_back(a);
  }
  bfs(1);
  return 0;
}

