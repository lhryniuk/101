#include <iostream>
#include <vector>
#include <queue>
using namespace std;

#define SIZE(r) int((r).size())
#define PB push_back

const int ROZ = 1000;

bool odwiedzony[ROZ];
queue<int> kol;
vector<int> G[ROZ];
int odleglosc[ROZ];
vector<int> mrow;
int poz[ROZ];

void wrzuc(int u)
{
    for(int i = 0; i < SIZE(G[u]); ++i)
    {
        if(!odwiedzony[G[u][i]])
        {
            kol.push(G[u][i]);
            odleglosc[G[u][i]] = odleglosc[u] + 1;
        }
        odwiedzony[G[u][i]] = true;
    }
}

void BFS(int v)
{
    odwiedzony[v] = true;
    odleglosc[v] = 0;
    kol.push(v);
    while(!kol.empty())
    {
        int u = kol.front();
        kol.pop();
        wrzuc(u);
    }
}

int main()
{
    ios_base::sync_with_stdio(0);
    int n;
    int a, b;
    cin >> n;
    for(int i = 0; i < n; ++i) odleglosc[i] = -1;
    int l = n;
    while(--l)
    {
        cin >> a >> b;
        G[a].PB(b);
        G[b].PB(a);
    }
    int k;
    cin >> k;
    mrow.PB(0);
    for(int i = 1; i <= k; ++i)
    {
        cin >> a;
        mrow.PB(a); // lisc, na ktorym siedzi mrowka
        poz[a] = i; // numer mrowki na lisciu a
    }
    int x;
    for(int i = 1; i <= n; ++i)
    {
        cout << i << ": ";
        x = G[i].size();
        for(int j = 0; j < x; ++j) cout << G[i][j] << ' ';
        cout << endl;
    }
    cout << "------------------\n";
    x = mrow.size() - 1;
    for(int i = 1; i <= x; ++i)
    {
        cout << mrow[i] << ' '; // wypisanie lisci kolejnych mrowek
    }
    cout << endl;
    for(int i = 1; i <= n; ++i)
    {
        cout << poz[i] << ' '; // wypisanie numerow mrowek na kolejnych lisciach
    }
    cout << endl;
    cin >> k;
    while(k--) // wczytywanie atakow biedronki
    {
        cin >> a;
    }
}
