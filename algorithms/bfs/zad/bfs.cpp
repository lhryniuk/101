#include <iostream>
#include <vector>
#include <queue>
using namespace std;

const int SIZE = 1000;

vector<int> G[SIZE];
queue<int> kolejka;
bool odwiedzony[SIZE];
int odleglosc[SIZE];

void wrzuc(int u)
{
    for(int i = 0; i < int(G[u].size()); ++i)
    {
        if(!odwiedzony[G[u][i]])
        {
            kolejka.push(G[u][i]);
            odleglosc[G[u][i]] = odleglosc[u] + 1;
        }
        odwiedzony[G[u][i]] = true;
    }
}

void BFS(int v)
{
    odwiedzony[v] = true;
    odleglosc[v] = 0;
    kolejka.push(v);
    while(!kolejka.empty())
    {
        int u = kolejka.front();
        kolejka.pop();
        wrzuc(u);
    }
}

int main()
{
    int m, n;
    int a, b;
    cin >> m >> n;
    for(int i = 0; i < m; ++i) odleglosc[i] = -1;
    while(n--)
    {
        cin >> a >> b;
        G[a].push_back(b);
        G[b].push_back(a);
    }
    cout << "Sasiedzi wierzcholka:" << endl;
    for(int i = 0; i <= m; ++i)
    {
        cout << i << ": ";
        for(int j = 0; j < int(G[i].size()); ++j) cout << G[i][j] << ' ';
        cout << endl;
    }
    int posz;
    cout << "Od ktorego wierzcholka chcesz wyznaczac odleglosci?" << endl;
    cin >> posz;
    BFS(posz);
    cout << "Odleglosci od wierzcholka " << posz << ":" << endl;
    for(int i = 0; i <= m; ++i)
    {
        cout << i << ":" << odleglosc[i] << endl;
    }
}
