#include <iostream>
#include <vector>
#include <queue>
using namespace std;

/*
 * Czysty BFS (pozniej, gdy to zrozumiesz,
 * bedziesz mogl nauczyc sie liczenia odleglosc, itd.)
 *
 * Po wczytaniu krawedzi, wywolujemy BFS-a, ktory dziala tak:
 *   * V    - vector booli z zapisem, ktore wierzcholki juz odwiedzilismy
 *   * G    - vector vectorow z zapisem wierzcholkow sasiadujacych z danym
 *   * G[k] - vector zawierajacy sasiadow wierzcholka k
 *   * Q    - kolejka zawierajaca numery wierzcholkow do przetworzenia
 *       
 *       void BFS(int v);
 *  1) jezeli przetwarzany wierzcholek byl juz odwiedzony:
 *     # wychodzimy z funkcji 
 *       return;
 *  
 *  2) oznaczamy wierzcholek jako odwiedzony
 *       V[v] = true;
 *  
 *  3) wrzucamy wierzcholek do kolejki przetwarzania
 *       Q.push(v);
 *  
 *  4) dopoki kolejka nie jest pusta:
 *       while(!Q.empty())
 *
 *    a) odczytujemy pierwszy element z kolejki i go z niej usuwamy
 *       (bedziemy go przetwarzac)
 *       int u = Q.front();
 *       Q.pop();
 *    
 *    b) przegladamy jego sasiadow i...
 *       for (int i = 0; i < int(G[u].size()); ++i)
 *      
 *       ################ UWAGA #################
 *       # mozemy przypisac t = G[u][i] w petli #
 *       # i mamy wtedy odpowiednio:            #
 *       # if (!V[t])                           #
 *       # Q.push(t);                           #
 *       # V[t] = true;                         #
 *       ########################################
 *
 *       jezeli nie odwiedzilismy danego wierzcholka
 *       if (!V[G[u][i]]) // G[u][i] to i-ty sasiad wierzcholka u
 *
 *       zapisujemy go do pozniejszego przetworzenia (wrzucamy do kolejki)
 *       Q.push(G[u][i]);
 *
 *       ...i oznaczamy jako odwiedzony
 *       V[G[u][i]] = true;
 *
 *  Dzieki zastosowaniu kolejki przetwarzamy najpierw dany wierzcholek (np. v),
 *  nastepnie jego sasiadow, dalej sasiadow sasiadow (bez v), itd.
 */

vector<vector<int> > G;
vector<bool> V;
queue<int> Q;

void bfs(int v)
{
  if (V[v]) return;
  V[v] = true;
  Q.push(v);
  while (!Q.empty()) {
    int u = Q.front();
    Q.pop();
    for (int i = 0; i < int(G[u].size()); ++i) { /* sasiedzi wierzcholka u */
      if (!V[G[u][i]]) {
        Q.push(G[u][i]);
        V[G[u][i]] = true;
      }
    }
  }
}

int main()
{
  ios_base::sync_with_stdio(0);
  int n, m; /* n - liczba wierzcholkow
             * m - liczba krawedzi */
  cin >> n >> m;
  G.resize(n+1); /* powiekszamy vector do odpowiedniej wielkosci */
  V.resize(n+1, false); /* jw. i wypelniamy ten vector wartoscia false, 
                         * bo na poczatku zaden nie jest odwiedzony */
  for (int i = 0; i < m; ++i) { /* wczytujemy krawedzie */
    int a, b;
    cin >> a >> b; /* wierzcholki a i b sasiaduja ze soba */
    G[a].push_back(b); /* robimy co nalezy, b wpisujemy jako sasiada a */
    G[b].push_back(a); /* ...a wpisujemy jako sasiada b */
  }
  bfs(1); /* wykonujemy bfs #tylko# na wierzcholku nr 1 (nie odwiedzamy
           * wierzcholkow, do ktorych nie mozna dojsc od niego 
           * po krawedziach */
  return 0;
}

