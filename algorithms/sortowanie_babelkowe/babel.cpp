#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

const int ROZ = 10;
int tab[ROZ];

void losuj(int tab[], int r)
{
    for(int i = 0; i < r; ++i) tab[i] = rand() % (r * 2);
}

void wypisz(int tab[], int r)
{
    for(int i = 0; i < r; ++i)
        cout << tab[i] << ' ';
    cout << endl;
}

void bubble_sort(int tab[], int r)
{
    for(int i = 1; i < r; ++i)
    {
        for(int j = r-1; j >= i; --j)
        {
            if(tab[j] < tab[j-1])
                swap(tab[j-1], tab[j]);
        }
        wypisz(tab, ROZ);
    }
}

int main()
{
    losuj(tab, ROZ);
    wypisz(tab, ROZ);
    bubble_sort(tab, ROZ);
    wypisz(tab, ROZ);
}
