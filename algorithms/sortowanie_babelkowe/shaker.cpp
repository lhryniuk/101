#include <iostream>
#include <ctime>
#include <cstdlib>
using namespace std;

const int ROZ = 10;
int tab[ROZ];

void losuj(int tab[], int r)
{
    for(int i = 0; i < r; ++i) tab[i] = rand() % (r * 2);
}

void wypisz(int tab[], int r)
{
    for(int i = 0; i < r; ++i)
        cout << tab[i] << ' ';
    cout << endl;
}

void shaker_sort(int tab[], int r)
{
    int left = 1, right = r - 1, k = r - 1;
    do
    {
        for(int j = right; j >= left; j--)
        {
            if(tab[j-1] > tab[j])
            {
                swap(tab[j-1], tab[j]);
                k = j;
            }
        }
        left = k + 1;
        for(int j = left; j <= right; ++j)
        {
            if(tab[j-1] > tab[j])
            {
                swap(tab[j-1], tab[j]);
                k = j;
            }
        }
        right = k - 1;
    }while (left <= right);

}

int main()
{
    losuj(tab, ROZ);
    wypisz(tab, ROZ);
    shaker_sort(tab, ROZ);
    wypisz(tab, ROZ);
}
