#include <iostream>
#include <cmath>
using namespace std;

struct POINT
{
        double x;
        double y;
        POINT(double x=0, double y=0) : x(x), y(y) { }
};

double D(POINT p, POINT p1, POINT p2)
{
        // (y - y0) = A / B * (x - x0)
        // B * (y - y0) + A * (x0 - x)
        double A = p2.y - p1.y, B = p2.x - p1.x;
        return abs(A * (p1.x - p.x) + B * (p.y - p1.y)) / sqrt(A * A + B * B);
}

int main()
{
        POINT a, b, c;
        cin >> a.x >> a.y;
        cin >> b.x >> b.y;
        while (1) {
                cin >> c.x >> c.y;
                cout << D(c, a, b) << endl;
        }
}
