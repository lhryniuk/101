package recfun

object test {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(326); 
  def countChange(money: Int, coins: List[Int]): Int = {
    
    def count(money: Int, coins: List[Int]): Int = {
      if (money == 0) 1
      else if (money < 0 || coins.isEmpty) 0
      else  count(money - coins.head, coins) + count(money, coins.tail)
    }
      
    count(money, coins)
  };System.out.println("""countChange: (money: Int, coins: List[Int])Int""");$skip(32); val res$0 = 
  countChange(4, List(1, 2, 5));System.out.println("""res0: Int = """ + $show(res$0))}
}
