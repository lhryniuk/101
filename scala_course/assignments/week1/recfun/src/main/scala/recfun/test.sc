package recfun

object test {
  def countChange(money: Int, coins: List[Int]): Int = {
    
    def count(money: Int, coins: List[Int]): Int = {
      if (money == 0) 1
      else if (money < 0 || coins.isEmpty) 0
      else  count(money - coins.head, coins) + count(money, coins.tail)
    }
      
    count(money, coins)
  }                                               //> countChange: (money: Int, coins: List[Int])Int
  countChange(4, List(1, 2, 5))                   //> res0: Int = 3
}