package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c == r || c == 0 || r < c) 1
    else pascal(c, r -1) + pascal(c - 1, r - 1)
    
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {
    
    def parCounter(x: Int, chars: List[Char]): Boolean =
      if (x < 0) false
      else if (chars.isEmpty) x == 0
      else if (chars.head == '(') parCounter(x+1, chars.tail)
      else if (chars.head == ')') parCounter(x-1, chars.tail)
      else parCounter(x, chars.tail)
    
    parCounter(0, chars)
  }

  /**
    * Exercise 3
    */
  def countChange(money: Int, coins: List[Int]): Int = {
    def count(money: Int, coins: List[Int]): Int = {
      if (money == 0) 1
      else if (money < 0 || coins.isEmpty) 0
      else count(money - coins.head, coins) + count(money, coins.tail)
    }
    
    if (money == 0) 0 else count(money, coins)
  }
}
