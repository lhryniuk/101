package week1

import scala.annotation.tailrec

object factorial {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(142); 

	def f(n: Int, s: Int, b: Int): Int = if (b > n) s else f(n, s * b, b + 1);System.out.println("""f: (n: Int, s: Int, b: Int)Int""");$skip(13); val res$0 = 
  f(5, 1, 1);System.out.println("""res0: Int = """ + $show(res$0))}

}
