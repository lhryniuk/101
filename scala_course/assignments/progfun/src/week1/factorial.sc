package week1

import scala.annotation.tailrec

object factorial {

	def f(n: Int, s: Int, b: Int): Int = if (b > n) s else f(n, s * b, b + 1)
                                                  //> f: (n: Int, s: Int, b: Int)Int
  f(5, 1, 1)                                      //> res0: Int = 120

}