package funsets

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {


  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }

  
  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }
  
  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   * 
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   * 
   *   val s1 = singletonSet(1)
   * 
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   * 
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   * 
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   * 
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {
    
    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3". 
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton")
    }
  }

  test("union contains all elements") {
    new TestSets {
      val s = union(s1, s2)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
    }
  }

  trait IntSets {
    def pos(x: Int): Boolean = x > 0
    def neg(x: Int): Boolean = x < 0
    def more_then_ten(x: Int): Boolean = x > 10
  }

  test("union non-zero") {
    new IntSets {
      val s = union(pos, neg) 
      assert(contains(s, -999), "Union 4")
      assert(contains(s, -100), "Union 5")
      assert(contains(s, -1), "Union 6")
      assert(contains(s, 1), "Union 7")
      assert(contains(s, 10), "Union 8")
      assert(contains(s, 999), "Union 9")
      assert(!contains(s, 0), "Union 10")
    } 
  }

  test("empty intersect") {
    new IntSets {
      val s = intersect(pos, neg) 
      assert(!contains(s, 0), "Intersect 1")
      assert(!contains(s, 5), "Intersect 2")
      assert(!contains(s, 7), "Intersect 3")
      assert(!contains(s, -999), "Intersect 4")
      assert(!contains(s, -100), "Intersect 5")
      assert(!contains(s, -1), "Intersect 6")
      assert(!contains(s, 1), "Intersect 7")
      assert(!contains(s, 10), "Intersect 8")
      assert(!contains(s, 999), "Intersect 9")
      assert(!contains(s, 0), "Intersect 10")
    } 
  }

  test("intersect more then ten") {
    new IntSets {
      val s = intersect(pos, more_then_ten) 
      assert(contains(s, 15), "Intersect 11")
      assert(contains(s, 11), "Intersect 12")
      assert(contains(s, 999), "Intersect 13")
      assert(!contains(s, 10), "Intersect 14")
      assert(!contains(s, 5), "Intersect 15")
      assert(!contains(s, -999), "Intersect 16")
      assert(!contains(s, -100), "Intersect 17")
      assert(!contains(s, -1), "Intersect 18")
      assert(!contains(s, 1), "Intersect 19")
      assert(!contains(s, 0), "Intersect 20")
    } 
  }

  test("diff") {
    new IntSets {
      val s = diff(pos, more_then_ten) 
      assert(contains(s, 10), "Diff 1")
      assert(contains(s, 5), "Diff 2")
      assert(contains(s, 1), "Diff 3")
      assert(contains(s, 3), "Diff 4")
      assert(!contains(s, 15), "Diff 5")
      assert(!contains(s, 11), "Diff 6")
      assert(!contains(s, -999), "Diff 7")
      assert(!contains(s, -100), "Diff 8")
      assert(!contains(s, -1), "Diff 9")
      assert(!contains(s, 0), "Diff 10")
    } 
  }

  test("filter positive even") {
    new IntSets {
      val s = filter(pos, x => x % 2 == 0) 
      assert(contains(s, 10), "Filter even 1")
      assert(contains(s, 312), "Filter even 2")
      assert(contains(s, 888), "Filter even 3")
      assert(contains(s, 38), "Filter even 4")
      assert(contains(s, 2), "Filter even 5")
      assert(!contains(s, 5), "Filter even 6")
      assert(!contains(s, 991), "Filter even 7")
      assert(!contains(s, 83), "Filter even 8")
      assert(!contains(s, 315), "Filter even 9")
      assert(!contains(s, 11), "Filter even 10")
      assert(!contains(s, -999), "Filter even 11")
      assert(!contains(s, -100), "Filter even 12")
      assert(!contains(s, -1), "Filter even 13")
      assert(!contains(s, 0), "Filter even 14")
    } 
  }

  test("filter negative odd") {
    new IntSets {
      val s = filter(neg, x => x % 2 == 1 || x % 2 == -1) 
      assert(!contains(s, 10), "Filter odd 1")
      assert(!contains(s, 312), "Filter odd 2")
      assert(!contains(s, 888), "Filter odd 3")
      assert(!contains(s, 38), "Filter odd 4")
      assert(!contains(s, 2), "Filter odd 5")
      assert(!contains(s, 5), "Filter odd 6")
      assert(contains(s, -991), "Filter odd 7")
      assert(contains(s, -83), "Filter odd 8")
      assert(!contains(s, 315), "Filter odd 9a")
      assert(contains(s, -315), "Filter odd 9b")
      assert(!contains(s, 11), "Filter odd 10")
      assert(contains(s, -999), "Filter odd 11")
      assert(contains(s, -101), "Filter odd 12")
      assert(contains(s, -1), "Filter odd 13")
      assert(!contains(s, 0), "Filter odd 14")
    } 
  }

  test("forall") {
    new IntSets {
      val s = intersect(pos, more_then_ten) 
      assert(!forall(s, x => x > 11), "Forall 1")
      assert(forall(s, x => x > 10), "Forall 2")
      assert(forall(s, x => x > 5), "Forall 3")
      assert(!forall(pos, x => x > 5), "Forall 4")
      assert(!forall(pos, x => x > 5), "Forall 5")
      assert(forall(pos, x => x > 0), "Forall 6")
      assert(forall(neg, x => x < 0), "Forall 7")
      assert(forall(neg, x => x < 133), "Forall 8")
    } 
  }

  test("exists") {
    new IntSets {
      assert(!exists(more_then_ten, x => x == 10), "Exists 1")
      assert(exists(pos, x => x > 17), "Exists 2")
      assert(exists(pos, x => x > 100), "Exists 3")
      assert(!exists(pos, x => x < 0), "Exists 4")
      assert(!exists(pos, x => x % 10000 == 1001), "Exists 5")
      assert(exists(neg, x => x < -999), "Exists 6")
      assert(exists(neg, x => x == -13), "Exists 7")
      assert(exists(neg, x => x < 100000), "Exists 8")
    } 
  }

  test("map") {
    new IntSets {
      val even = map(pos, x => x * 2)
      val fourth = map(neg, x => x * 4)
      val seventh = map(pos, x => x * 7)
      assert(contains(even, 432), "Map 1")
      assert(contains(even, 132), "Map 2")
      assert(!contains(fourth, 103), "Map 3")
      assert(contains(fourth, -100), "Map 4")
      assert(!contains(fourth, 222), "Map 5")
      assert(contains(seventh, 777), "Map 6")
      assert(contains(seventh, 14), "Map 7")
      assert(!contains(seventh, -14), "Map 8")
      assert(!contains(seventh, 241), "Map 9")
    } 
  }
}
