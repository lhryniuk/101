package forcomp

object comb {
  type Occurrences = List[(Char, Int)];import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(113); 
  println("Welcome to the Scala worksheet");$skip(35); 
  val a = List(('a', 2), ('b', 2));System.out.println("""a  : List[(Char, Int)] = """ + $show(a ));$skip(85); 
	def removeZeros(occ: Occurrences): Occurrences = {
			occ filter (o => o._2 > 0)
	};System.out.println("""removeZeros: (occ: forcomp.comb.Occurrences)forcomp.comb.Occurrences""");$skip(67); 
	def subOne(p: (Char, Int)): (Char, Int) = {
		(p._1, p._2 - 1)
	};System.out.println("""subOne: (p: (Char, Int))(Char, Int)""");$skip(204); 
	def comb(occ: Occurrences): List[Occurrences] = occ match {
		case Nil => List(List())
		case x::xs => {
			for {
				y <- comb(xs)
				i <- 1 to x._2
			} yield (x._1, i) :: y
		}.toList ++ comb(xs)
	};System.out.println("""comb: (occ: forcomp.comb.Occurrences)List[forcomp.comb.Occurrences]""");$skip(11); val res$0 = 
	
	comb(a)
   type Word = String;System.out.println("""res0: List[forcomp.comb.Occurrences] = """ + $show(res$0));$skip(178); 
  def wordOccurrences(w: Word): Occurrences = {
    ((w.toList.groupBy((e: Char) => e.toLower)) map {case (k, v) => (k, v.length)}).toList.sortBy(_._1)
  };System.out.println("""wordOccurrences: (w: forcomp.comb.Word)forcomp.comb.Occurrences""");$skip(27); val res$1 = 
  wordOccurrences("Hello");System.out.println("""res1: forcomp.comb.Occurrences = """ + $show(res$1))}
	//a.permutations
}
